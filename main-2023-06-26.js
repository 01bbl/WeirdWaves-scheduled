/*jshint esversion: 6*/

/*
SCRIPT CONTROLS: an interface to change the app's behaviour (only really useful if running your own)
PARAMETERS: internal parameters (probably don't need to be changed)
FUNCTIONS
	TIMING: transforming and writing timestamps
	INITIALISATION: initialising station and setting event clock
	SCHEDULE: getting schedules, filling them in, and loading data onto the page
	TUNING: tuning in and out
	SHOWS: loading and starting shows, and starting intermissions
	INTERFACE: audio controls and settings
	PAGE CONSTRUCTION: building page sections and content (e.g. the Archive)
	DISPLAY: updating site display according to display (style and font) settings
EVENTS: event listeners
ARCHIVE: an object with every source and show in the Archive
*/



/* ====================
	SCRIPT CONTROLS
==================== */

// relative path of the schedules folder and show audio folder
const schedulePath = "./schedules/schedule-",
showPath = "./audio/shows/";

// the first date of broadcast (in the station's timezone) and number of days the station's been offline
const broadcastStartDate = new Date("2022-06-25"),
daysOffline = 0;

// if you want to adjust the station to your timezone, enter your timezone (relative to UTC/GMT+0) here
const stationTimezoneOffset = 0;
// EXAMPLE with stationTimezoneOffset = -8: the schedule will start (and roll over) at midnight in UTC-8 timezone.

const settings = window.localStorage.getItem("settings") ? JSON.parse(window.localStorage.getItem("settings")) : {
	"muteTuneIn": false,
	"notesOpen": false,
	"primetimeOn": false,
	"primetimeStart": 0,
	"primetimeEnd": 0
};

// timeout parameters for getting the schedule and tuning into a show: Time is the number of ms each attempt takes and Max is the number of attempts made before timing out
const timeoutTime = 100,
timeoutMax = 600;

// maximum number of shows displayed on the upcoming forecast on the landing page
const recentNewsMaxLength = 3,
upcomingForecastMaxLength = 6;

// add a default schedule in case the script can't find the weekly schedule, and set whether the station should always use the default instead of checking for a weekly schedule file
const defaultSchedule = {
"info": {
	"title": "Default",
	"description": "This is the player's default schedule if it can't find a weekly schedule file. No common theme, just some strong shows!",
	"weekdayShift": 2,
	"weekendShift": 4
},
"weekday": [
	{"id": "QP-060"},
	{"id": "Sus-674"},
	{"id": "Mw-003"},
	{"id": "QP-085"},
	{"id": "LO-A040-A072-C09"},
	{"id": "QP-001"},
	{"id": "XMO-071"},
	{"id": "PC-11"}
],
"weekend": [
	{"id": "Sus-094"},
	{"id": "SET-43-44"},
	{"id": "Sus-059"},
	{"id": "QP-092"},
	{"id": "Mw-026"},
	{"id": "QP-040"},
	{"id": "Sus-222"},
	{"id": "LV-05"}
]
};

// set whether the station is on hiatus and add a hiatus schedule to repeat each week if so
const onHiatus = true,
hiatusSchedule = {
"info": {
	"title": "Hiatus Bangers",
	"description": "The weekly Weird Waves radio is on hiatus&mdash;instead, enjoy a selection of some of the best and most interesting shows in the Archive!",
	"weekdayShift": 5,
	"weekendShift": 12
},
"weekday": [
	{"id": "Mw-012"},
	{"id": "CAS-4"},
	{"id": "Sus-094"},
	{"id": "Mw-147"},
	{"id": "QP-001"},
	{"id": "LV-46"},
	{"id": "Esc-015"},
	{"id": "Sus-674"},
	{"id": "QP-092"},
	{"id": "CRW-66"},
	{"id": "HF-C12"},
	{"id": "Sus-346"},
	{"id": "DX-26"},
	{"id": "Mw-057"},
	{"id": "BC-X"},
	{"id": "Sus-222"},
	{"id": "CW-A034"},
	{"id": "BkM-07"},
	{"id": "Nf-034"},
	{"id": "RCP-58"},
	{"id": "McT-17a"},
	{"id": "McT-17b"},
	{"id": "TF-154"},
	{"id": "SET-43-44"}
],
"weekend": [
	{"id": "QP-085"},
	{"id": "DX-50"},
	{"id": "XMO-071"},
	{"id": "PC-11"},
	{"id": "Mw-001"},
	{"id": "Esc-052"},
	{"id": "Mw-003"},
	{"id": "LV-02"},
	{"id": "CW-D27"},
	{"id": "LO-A040-A072-C09"},
	{"id": "QP-037"},
	{"id": "ByM-20"},
	{"id": "Sus-059"},
	{"id": "Mw-082"},
	{"id": "SET-29"},
	{"id": "QP-060"},
	{"id": "LV-07"},
	{"id": "XMO-068"},
	{"id": "SNM-15"},
	{"id": "Nf-018"},
	{"id": "CAS-5"},
	{"id": "SET-12"},
	{"id": "SET-13"},
	{"id": "MsT-111"}
]
};



/* ===============
	PARAMETERS
=============== */

// HTML elements
const page = {
	"radio": document.getElementById("radio-main"),

	"intermissionNote": document.getElementById("intermission-note"),
	"tuneInButton": document.getElementById("tune-in-button"),
	"radioLoadIcon": document.getElementById("loading-spinner-radio"),
	"loadedShow": document.getElementById("loaded-show"),
	"muteButton": document.getElementById("mute-button"),
	"volumeControl": document.getElementById("volume-control"),
	"tuneOutButton": document.getElementById("tune-out-button"),

	"forecastTables": document.getElementById("forecast-tables"),

	"muteTuneInToggle": document.getElementById("mute-tune-in-toggle"),
	"contentNotesToggle": document.getElementById("content-notes-toggle"),
	"themeButtons": document.querySelectorAll("#theme-buttons button"),
	"fontButtons": document.querySelectorAll("#font-buttons button"),
	"positionButtons": document.querySelectorAll("#position-buttons button"),
	"primetimeToggle": document.getElementById("primetime-toggle"),
	"primetimeStartInput": document.getElementById("primetime-start"),
	"primetimeEndInput": document.getElementById("primetime-end"),
	"primetimeUpdate": document.getElementById("primetime-update")
};

const audio = {
	"show": document.getElementById("show"),
	"intermission": document.getElementById("intermission")
};

// initialise schedule
let schedule;

// when running script on pageload (i.e. before tuning in for the first time), set this to false
let tunedIn = false;

// timeout counters for getting schedules and loading into shows (on first tuning in)
const timeoutCounter = {
	"schedule": 0,
	"loadedShow": 0
};



/* ==============
	FUNCTIONS
============== */

/* ---
TIMING
--- */

// get station's current time in its timezone
function getStationTime() {
	const timestamp = new Date();
	timestamp.setHours(timestamp.getHours() + stationTimezoneOffset);
	return timestamp;
}

// get ordinal for "nice" timestamp
function writeOrdinalDate(date) {
	let ordinal;

	if (date > 3 && date < 21) ordinal = "th";
	else {
		switch(date % 10) {
		case 1: ordinal = "st"; break;
		case 2: ordinal = "nd"; break;
		case 3: ordinal = "rd"; break;
		default: ordinal = "th";
		}
	}
	return String(date) + ordinal;
}

// write a "nice" timestamp
function writeNiceTime(time) {
	const dayNames = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
	monthNames = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];

	return dayNames[time.getDay()] + " " + writeOrdinalDate(time.getDate()) + " " + monthNames[time.getMonth()];
}

// add 1 leading zero to 1-digit dates/times
function padTime(timeNum) {
	return (timeNum < 10) ? "0" + timeNum : String(timeNum);
}

/* -----------
INITIALISATION
----------- */

// if player has gotten schedule file, load the schedule onto the page, allow user to tune in, and start checking for station updates; otherwise, try again after 100 ms
// (note: although this function is only used once outside of itself, because it needs to call itself in a timeout, it must be a separate, discrete function)
function initialiseStation() {
	if (typeof schedule == "undefined") {
		if (timeoutCounter.schedule < timeoutMax) {
			setTimeout(initialiseStation, timeoutTime);
			timeoutCounter.schedule += 1;
			console.log("waiting for schedule, check " + timeoutCounter.schedule);
		} else {
			console.log("timed out while waiting for schedule");
			page.radio.textContent = "Sorry, your browser timed out while trying to get the schedule.";
		}
	} else {
		console.log("initialising station");

		timeoutCounter.schedule = 0;

		page.radioLoadIcon.classList.add("hidden");
		page.tuneInButton.classList.remove("hidden");
		loadSchedule();

		updateStation();
		setInterval(updateStation, 1000);
	}
}

// check whether to: start next show, get next week's schedule, and/or load next week's schedule
function updateStation() {
	const currentTime = getStationTime(),
	futureTime = new Date(currentTime.getTime()); // if something messes up with this function, replace this with futureTime = getStationTime();

	futureTime.setHours(futureTime.getHours() + 1);

// at the start of each hour, start the next show (if tuned in) and remove the previous show from the forecast (if using dynamic forecast), including removing empty forecast tables
	if (currentTime.getMinutes() === 0 && currentTime.getSeconds() === 0) {
		if (tunedIn) startShow();
		page.forecastTables.querySelector("tr")?.remove();
		if (currentTime.getHours() === 0) page.forecastTables.firstElementChild?.remove();
	}
// if the time is within an hour of the start of the next schedule, get that schedule
	if (futureTime.getUTCDay() === 1 && futureTime.getUTCDate() !== schedule.info.date) {
		console.log("getting next schedule");
		getSchedule(futureTime, false);
	}
// at the start of the next schedule, load in that schedule
	if (currentTime.getMinutes() === 0 && currentTime.getSeconds() === 0 && currentTime.getUTCHours() === 0 && currentTime.getUTCDay() === 1) loadSchedule();
}

/* -----
SCHEDULE
----- */

// fetch the current week's schedule and fill it out with show info from the archive (add schedule info to page if it's the first schedule of this pageload)
function getSchedule(time, tuningIn) {
	const weekStart = time,
	daysSinceStart = (time.getUTCDay() + 6) % 7;

	weekStart.setUTCDate(weekStart.getUTCDate() - daysSinceStart);

	const scheduleURL = schedulePath + time.getUTCFullYear() + "-" + padTime(time.getUTCMonth()+1) + "-" + padTime(weekStart.getUTCDate()) + ".json";

	if (!onHiatus) {
		fetch(scheduleURL)
		.then(response => response.json())
		.then(data => {
			schedule = data;
			expandSchedule(weekStart);
			console.log("getting schedule " + scheduleURL);
		})
		.catch(() => {
			schedule = defaultSchedule;
			expandSchedule(weekStart);
			console.log("getting default schedule");
		});
	} else {
		schedule = hiatusSchedule;
		expandSchedule(weekStart);
		console.log("getting hiatus schedule");
	}

	if (tuningIn) initialiseStation();
}

// fill out schedule info from the skeleton in the schedule object
function expandSchedule(weekStart) {
	importShowInfoToSchedule('weekday');
	importShowInfoToSchedule('weekend');

	schedule.info.date = weekStart.getUTCDate();
}

// add a show block's show data to the schedule object (do this once when a new/non-local schedule is loaded to avoid crawling the DOM every time a show is loaded)
function importShowInfoToSchedule(blockName) {
	for (const show of schedule[blockName]) {
		const showID = show.id;
		showInArchive = document.getElementById(showID),
		seriesInArchive = showInArchive.parentElement.parentElement;

		show.file = showInArchive.querySelector(".audio-link").href;
		show.series = seriesInArchive.querySelector(".series-heading").innerHTML;
		show.title = showInArchive.querySelector(".show-heading").innerHTML;
		show.info = showInArchive.querySelector(".show-info").innerHTML;
		show.source = seriesInArchive.querySelector(".series-source").outerHTML;
	}
}

// write schedule parts onto page
function loadSchedule() {
	console.log("loading schedule: " + schedule.info.title.replace(/<\/?[^>]+(>|$)/g, ""));

	for (const title of document.querySelectorAll(".schedule-title")) title.innerHTML = schedule.info.title;
	for (const description of document.querySelectorAll(".schedule-description")) description.innerHTML = schedule.info.description;

	document.getElementById("loading-spinner-schedule")?.remove();
	document.getElementById("loading-spinner-schedule-summary")?.remove();

	const time = new Date(),
	weekdayStart = document.getElementById("weekday-start"),
	weekendStart = document.getElementById("weekend-start");

	if (time.getUTCDate() < schedule.info.date) time.setMonth(time.getMonth() - 1); // if schedule crosses from one month to the next, set the timestamp back to the previous month
	time.setDate(schedule.info.date); // set initial timestamp to the schedule's start date
	weekdayStart.textContent = writeNiceTime(time);
	weekdayStart.setAttribute("datetime", time.toISOString().substring(0,10));
	document.getElementById("schedule-weekday").innerHTML = writeScheduleList(schedule.weekday);

	time.setDate(time.getDate() + 5); // advance timestamp to the weekend after the schedule's start date
	weekendStart.textContent = writeNiceTime(time);
	weekendStart.setAttribute("datetime", time.toISOString().substring(0,10));
	document.getElementById("schedule-weekend").innerHTML = writeScheduleList(schedule.weekend);

// honour stored notes visibility setting (if set)
	const scheduleNotes = document.querySelectorAll("#schedule .content-notes");
	for (const notes of scheduleNotes) notes.open = settings.notesOpen;

	writeForecastTables();

	writeLandingPage();
}

// write out a schedule block as a list of metadata
function writeScheduleList(block) {
	let list = "";
	for (const show of block) {
		list += '<li><h4 class="show-heading">' + show.series + " " + show.title + '</h4>' +
'<div class="show-info">' + show.info + show.source + '</div>';
	}
	return list;
}

// write tables for each day's schedule
function writeForecastTables() {
	const time = new Date(),
	scheduleStart = new Date(time.getTime()),
	forecastLength = 7 * 24;

	let show = 0,
	rows = "",
	tables = "",
	primetimeClass,
	showInfo;

	if (time.getUTCDate() < schedule.info.date) scheduleStart.setMonth(scheduleStart.getMonth() - 1); // if schedule crosses from one month to the next, set the scheduleStart timestamp back to the previous month

	scheduleStart.setDate(schedule.info.date);
	scheduleStart.setHours(0 - stationTimezoneOffset - time.getTimezoneOffset() / 60);
	scheduleStart.setMinutes(0);
	scheduleStart.setSeconds(0);

// adjust schedule start time to account for schedule starting in one timezone and ending in another
	const timezoneDifference = scheduleStart.getTimezoneOffset() - time.getTimezoneOffset();
	if (timezoneDifference > 0) { // regular time to DST
		scheduleStart.setHours(scheduleStart.getHours() + 1);
	} else if (timezoneDifference < 0) { // DST to regular time
		scheduleStart.setHours(scheduleStart.getHours() - 1);
	}

// set initial show to current show
	show = Math.floor((time - scheduleStart) / (1000 * 60 * 60));
	let daysSinceStart = Math.floor(show / 24);

	while (show < forecastLength) {
		const stationHour = show % 24;

		if (show < 5 * 24) showInfo = schedule.weekday[(stationHour + schedule.info.weekdayShift * daysSinceStart) % schedule.weekday.length];
		else showInfo = schedule.weekend[(stationHour + schedule.info.weekendShift * (daysSinceStart - 5)) % schedule.weekend.length];

		show += 1;
		if (show % 24 === 0) daysSinceStart += 1;

		rows += '<tr><th scope="row"><time>' + padTime(time.getHours()) + ":00</time></th>" +
		"<td>" + showInfo.series + " " + showInfo.title + "</td></tr>";

		// at the end of each local day, or the whole schedule, build the day's heading and table
		if (time.getHours() === 23 || show === forecastLength) {
			tables += '<li><table class="time-table">' +
			'<caption><time datetime="' + time.toISOString().substring(0,10) + '">' + writeNiceTime(time) + "</time></caption>" +
			"<tbody>" + rows + "</tbody></table></li>";
			rows = "";
		}
		time.setHours(time.getHours() + 1);
	}

	document.getElementById("loading-spinner-forecast")?.remove();
	page.forecastTables.innerHTML = tables;

	markForecastPrimetime();
}

// mark/unmark primetime shows on the forecast
function markForecastPrimetime() {
	const primetimeHours = [],
	forecastDays = document.getElementById("forecast-tables").children;

	if (settings.primetimeStart >= settings.primetimeEnd) settings.primetimeEnd += 24;
	for (let i = settings.primetimeStart; i < settings.primetimeEnd; i++) primetimeHours.push(i % 24);

	for (let i = 0; i < forecastDays.length; i++) {
		const forecastHours = forecastDays[i].querySelectorAll("td"),
		startingHour = i > 0 ? 0 : 24 - forecastHours.length;
		
		for (let j = 0 + startingHour; j < forecastHours.length + startingHour; j++) {
			forecastHours[j - startingHour].classList.toggle("primetime-show", settings.primetimeOn && primetimeHours.includes(j));
		}
	}
}

/* ---
TUNING
--- */

// load in the station with the current show playing, set to the current time, and start checking whether to update the station
function tuneRadio() {
	if (tunedIn) {
		console.log("tuning out");

		audio.show.pause();
		audio.intermission.pause();

		page.radio.classList.add("hidden");
		page.tuneInButton.classList.remove("hidden");

		tunedIn = false;
	} else {
		console.log("tuning in");

		tunedIn = true;

		page.tuneInButton.classList.add("hidden");
		page.radioLoadIcon.classList.remove("hidden");

		const time = getStationTime();
		loadShow(time);
		tuneIntoShow(time);
	}
}

// if show duration has loaded, check whether to load into the show or intermission; otherwise, try again after 100 ms
function tuneIntoShow(time) {
	if (typeof audio.show.duration !== "number" || isNaN(audio.show.duration)) {
		if (timeoutCounter.loadedShow < timeoutMax) {
			setTimeout(tuneIntoShow, timeoutTime, time);
			timeoutCounter.loadedShow += 1;
			console.log("waiting for show metadata, check " + timeoutCounter.loadedShow);
		} else {
			console.log("timed out while waiting for show file");
			page.radio.textContent = "Sorry, your browser timed out while trying to tune in to the first show. Try refreshing the page.";
		}
	} else {
		timeoutCounter.loadedShow = 0;

		if (settings.muteTuneIn && !audio.show.muted) muteUnmuteAudio();
	
		const loadTime = time.getMinutes() * 60 + time.getSeconds();

		if (loadTime > audio.show.duration) {
			console.log("tuning in to intermission");
			startIntermission();
		} else {
			console.log("tuning in to show");
			audio.show.currentTime = loadTime;
			startShow();
		}

		page.radioLoadIcon.classList.add("hidden");
		page.radio.classList.remove("hidden");
	}
}

/* --
SHOWS
-- */

// write show parts onto page and load show audio file
function loadShow(showTime) {
	let showInfo,
	daysSinceStart = (showTime.getUTCDay() + 6) % 7;

	if (daysSinceStart < 5) showInfo = schedule.weekday[(showTime.getUTCHours() + schedule.info.weekdayShift * daysSinceStart) % schedule.weekday.length];
	else showInfo = schedule.weekend[(showTime.getUTCHours() + schedule.info.weekendShift * (daysSinceStart - 5)) % schedule.weekend.length];

	const showTitle = showInfo.series + " " + showInfo.title;

	page.loadedShow.innerHTML = '<h2 class="show-heading">' + showTitle + '</h2>' +
'<div class="show-info">' + showInfo.info + showInfo.source + '</div>';

	const radioNotesOpen = settings.notesOpen,
	loadedShowNotes = document.querySelector('#loaded-show .content-notes');
	if (loadedShowNotes) loadedShowNotes.open = radioNotesOpen;

	audio.show.src = showInfo.file;

	console.log("loading show: " + showTitle.replace(/<\/?[^>]+(>|$)/g, "")); // can't use .textContent because showTitle is a string, not a reference to an element
}

// start next show
function startShow() {
	console.log("starting show");

	audio.intermission.pause();
	audio.show.play();
	audio.show.autoplay = true;

	const time = new Date(),
	showUntil = document.createElement("span");
	showUntil.innerHTML = " (until <time>" + padTime(time.getHours()) + ":" + Math.ceil(audio.show.duration / 60) + "</time>)";
	showUntil.setAttribute("id", "show-until");

	document.getElementById("up-next")?.remove();
	page.loadedShow.querySelector(".show-heading").appendChild(showUntil);

	page.intermissionNote.classList.add("hidden");
}

// start intermission
function startIntermission() {
	console.log("starting intermission");

	audio.show.autoplay = false;
	audio.intermission.play();

	const futureTime = getStationTime();
	
	futureTime.setHours(futureTime.getHours() + 1);

	loadShow(futureTime);

	const localTime = new Date(),
	upNext = document.createElement("span");

	localTime.setHours(localTime.getHours() + 1);
	upNext.innerHTML = "Up next (<time>" + padTime(localTime.getHours()) + ":00</time>): ";
	upNext.setAttribute("id", "up-next");
	document.getElementById("show-until")?.remove();
	page.loadedShow.querySelector(".show-heading").prepend(upNext);

	page.intermissionNote.classList.remove("hidden");
}

/* ------
INTERFACE
------ */

// initialise a toggle switch with a stored or default value
function initialiseToggle(id, toggled) {
	document.getElementById(id).setAttribute("aria-pressed", toggled ? "true" : "false");
}

// switch a toggle from off/unpressed to on/pressed
function switchToggle(id, key, value) {
	const button = document.getElementById(id);

	if (button.getAttribute("aria-pressed") === 'false') button.setAttribute("aria-pressed", "true");
	else button.setAttribute("aria-pressed", "false");

	settings[key] = value;
}

// mute/unmute audio
function muteUnmuteAudio() {
	if (audio.show.muted) {
		audio.show.muted = false;
		audio.intermission.muted = false;
		page.muteButton.textContent = "Mute";
	} else {
		audio.show.muted = true;
		audio.intermission.muted = true;
		page.muteButton.textContent = "Unmute";
	}
}

// set audio volume
function setVolume(newVolume) {
	audio.show.volume = newVolume;
	audio.intermission.volume = newVolume;
}

// scroll to a series when clicking its link in the archive series nav list
function scrollToSeries(series) {
	document.getElementById("archive-" + series).focus(); // when Firefox adds :has(), remove this function and the tabindex from archive series' <h3>s created in buildArchive()
	document.getElementById("archive-" + series).scrollIntoView(true);
}

// toggle between muted (true) and unmuted (false) audio when tuning in
function toggleMuteTuneIn() {
	settings.muteTuneIn = !settings.muteTuneIn;
	switchToggle("mute-tune-in-toggle", 'muteTuneIn', settings.muteTuneIn);
}

// toggle between open (true) and closed (false) show content notes
function toggleContentNotes() {
	settings.notesOpen = !settings.notesOpen;
	switchToggle("content-notes-toggle", 'notesOpen', settings.notesOpen);

	const contentNotes = document.querySelectorAll(".content-notes");
	for (const notes of contentNotes) notes.open = settings.notesOpen;
}

// toggle between personal primetime being active (true) and inactive (false)
function togglePrimetime() {
	settings.primetimeOn = !settings.primetimeOn;
	switchToggle("primetime-toggle", 'primetimeOn', settings.primetimeOn);

	markForecastPrimetime();
}

/* --------------
PAGE CONSTRUCTION
-------------- */

// build archive onto page
function buildArchive() {
	let archiveHTML = "",
	archiveLinksHTML = "";

	for (const series of archive) {
		archiveLinksHTML += '<li><a href="#archive" data-series="' + series.code + '">' + series.heading + '</a></li>';

		archiveHTML += '<li><header>' +
'<h3 class="series-heading" id="archive-' + series.code + '" tabindex="0">' + series.heading + '</h3><div class="series-info">' +
series.blurb +
'<p class="series-source">source: ' + series.source + '</p></div>' +
'</header><ol class="show-list">';

		for (const show of series.shows) {
			archiveHTML += '<li id="' + series.code + '-' + show.code + '">' +
'<h4 class="show-heading">' + show.heading + '</h4>' +
'<div class="show-info">' + show.blurb;
			if (show.notes.length > 0) archiveHTML += '<details class="content-notes"><summary>Content notes </summary>' + show.notes + '</details>';
			archiveHTML += '</div><a class="audio-link" href="' + showPath + series.code + '-' + show.code + '-' + show.file + '.mp3">Listen</a></li>';
		}

		archiveHTML += '</ol></li>';
	}

	document.getElementById("loading-spinner-archive")?.remove();
	document.getElementById("series-list").innerHTML = archiveHTML;

	document.getElementById("loading-spinner-archive-links")?.remove();
	document.getElementById("archive-series-links").innerHTML = archiveLinksHTML;
	document.querySelectorAll("#archive-series-links a").forEach(link => link.addEventListener("click", () => setTimeout(scrollToSeries, 1, link.dataset.series)));
}

// count shows per series
function countShows() {
	const timeSinceBroadcastBegan = getStationTime() - broadcastStartDate,
	broadcastingDays = Math.floor(timeSinceBroadcastBegan / (1000 * 60 * 60 * 24)) - daysOffline;

	let totalCount = 0,
	showCountList = "";

	for (const series of archive) {
		const showCount = series.shows.length;

		totalCount += showCount;
		showCountList += "<dt>" + series.heading + "</dt><dd>" + showCount + "</dd>";
	}

	document.getElementById("loading-spinner-stats")?.remove();
	document.getElementById("stats-list").innerHTML = "<div><dt>days of broadcasting</dt><dd>" + broadcastingDays + "</dd></div>" +
"<div><dt>sources in the Archive</dt><dd>" + archive.length + "</dd></div>" +
"<div><dt>shows in the Archive</dt><dd>" + totalCount + "</dd></div>";

	document.getElementById("loading-spinner-show-counts")?.remove();
	document.getElementById("series-show-counts").innerHTML = showCountList;
}

// get random audio link from archive
function getRandomAudioLink() {
	const allAudioLinks = document.querySelectorAll(".audio-link");

	return allAudioLinks[Math.floor(Math.random() * allAudioLinks.length)].href;
}

// add info to landing page
function writeLandingPage() {
// get the X most recent news item titles and highlight any the visitor hasn't seen yet
	let recentNews = "";
	const newsHeadings = document.querySelectorAll("#news h3"),
	recentNewsLength = Math.min(recentNewsMaxLength, newsHeadings.length),
	newsReadDate = window.localStorage.getItem("newsReadDate") ? window.localStorage.getItem("newsReadDate") : 0;

	for (let i = 0; i < recentNewsLength; i++) {
		recentNews += Date.parse(newsHeadings[i].querySelector("time").getAttribute("datetime")) > newsReadDate - 1000*60*60*24 ? '<li class="unread-news">' : '<li>';
		recentNews += newsHeadings[i].innerHTML + "</li>";
	}
	document.getElementById("loading-spinner-recent-news")?.remove();
	document.getElementById("recent-news").innerHTML = recentNews;

	window.localStorage.setItem("newsReadDate", Date.parse(getStationTime()));

// get the next Y shows from the forecast
	let upcomingForecast = "";
	const forecastRows = document.querySelectorAll("#forecast tr"),
	upcomingForecastLength = Math.min(upcomingForecastMaxLength, forecastRows.length);
	for (let i = 0; i < upcomingForecastLength; i++) {
		upcomingForecast += forecastRows[i].outerHTML;
	}
	document.getElementById("loading-spinner-upcoming-forecast")?.remove();
	document.getElementById("upcoming-forecast").innerHTML = upcomingForecast;
}

/* ----
DISPLAY
---- */

// take reference to button element and string "true" or "false"
function setButtonState(button, state) {
	button.setAttribute("aria-pressed", state);
	if (state === "true") button.setAttribute("tabindex", "-1");
	else button.removeAttribute("tabindex");
}

// swap user-setting class on root element
function swapClass(settingName, newSetting) {
	for (const button of page[settingName+"Buttons"]) {
		setButtonState(button, button.dataset.setting === newSetting ? "true" : "false");
	}

	document.body.classList.remove(settingName+"-"+styles[settingName]);
	document.body.classList.add(settingName+"-"+newSetting);
}

// switch between different colour themes
function switchTheme(themeName) {
	swapClass("theme", themeName);
	styles.theme = themeName;
}

// switch between different font families (and stacks)
function switchFont(fontName) {
	swapClass("font", fontName);
	styles.font = fontName;
}

// switch radio area from left to right side of main content in widescreen view (1240px+)
function switchRadioPosition(position) {
	swapClass("position", position);
	styles.position = position;
}



/* ===========
	EVENTS
=========== */

// radio interface events
page.tuneInButton.addEventListener("click", () => tuneRadio());
page.tuneOutButton.addEventListener("click", () => tuneRadio());
page.muteButton.addEventListener("click", () => muteUnmuteAudio());
page.volumeControl.addEventListener("input", () => setVolume(page.volumeControl.value / 100));

// settings interface events (general)
page.muteTuneInToggle.addEventListener("click", () => toggleMuteTuneIn());
page.contentNotesToggle.addEventListener("click", () => toggleContentNotes());

// settings interface events (personal primetime)
page.primetimeToggle.addEventListener("click", () => togglePrimetime());
page.primetimeStartInput.addEventListener("focusout", () => {
	if (page.primetimeStartInput.checkValidity() && page.primetimeStartInput.value !== '') {
		settings.primetimeStart = Number(page.primetimeStartInput.value);
	}
});
page.primetimeEndInput.addEventListener("focusout", () => {
	if (page.primetimeEndInput.checkValidity() && page.primetimeEndInput.value !== '') {
		settings.primetimeEnd = Number(page.primetimeEndInput.value);
	}
});
page.primetimeUpdate.addEventListener("click", () => markForecastPrimetime());

// settings interface events (styling)
page.themeButtons.forEach(button => button.addEventListener("click", () => switchTheme(button.dataset.setting)));
page.fontButtons.forEach(button => button.addEventListener("click", () => switchFont(button.dataset.setting)));
page.positionButtons.forEach(button => button.addEventListener("click", () => switchRadioPosition(button.dataset.setting)));

// begin intermission at end of show
audio.show.addEventListener("ended", () => startIntermission());

// on pageload, execute various tasks
document.addEventListener("DOMContentLoaded", () => {

/* BUILD ARCHIVE */
	buildArchive();

/* SCHEDULE FETCH */
	getSchedule(getStationTime(), true);

/* PAGE MANIPULATION */
// set archive content notes status according to user preference/default setting
	const archiveNotes = document.querySelectorAll("#archive .content-notes");
	for (const notes of archiveNotes) notes.open = settings.notesOpen;

// write show count data list
	countShows();

// switch theme and font (during pageload this is mainly just pressing the matching buttons and marking the rest unpressed)
	switchTheme(styles.theme);
	switchFont(styles.font);
	switchRadioPosition(styles.position);

// add primetime settings if any are present
	page.primetimeStartInput.value = settings.primetimeStart;
	page.primetimeEndInput.value = settings.primetimeEnd;

// add random show href to href of random show link in nav bar
	document.getElementById("random-show-link").href = getRandomAudioLink();

/* BUTTON CONSTRUCTION */
// build out theme buttons
	for (const button of page.themeButtons) {
		button.innerHTML = '<span class="palette"><span style="background-color:#' + button.dataset.colours.split(" ").join('"></span><span style="background-color:#') + '"></span></span>' + button.innerText + '';
	}
// build out toggle buttons
	const toggleButtons = document.querySelectorAll(".toggle");

	for (const toggle of toggleButtons) {
		toggle.innerHTML = '<svg class="svg-icon" viewBox="0 0 48 24"><use href="#svg-toggle-track" /><g class="svg-toggle-thumb"><use href="#svg-toggle-thumb-side" /><use href="#svg-toggle-thumb-top" /><use href="#svg-toggle-tick" /></g></svg><span>' + toggle.innerText + '</span>';
	}

	initialiseToggle("mute-tune-in-toggle", settings.muteTuneIn);
	initialiseToggle("content-notes-toggle", settings.notesOpen);
	initialiseToggle("primetime-toggle", settings.primetimeOn);
});

// on closing window/browser tab, record user settings and styles to localStorage
window.addEventListener("unload", () => {
	window.localStorage.setItem("settings", JSON.stringify(settings));
	window.localStorage.setItem("styles", JSON.stringify(styles));
});



/* ============
	ARCHIVE
============ */

const archive = [
{
"code": "AOP",
"heading": "<cite>Arch Oboler's Plays</cite>",
"blurb": "<p>A 1939&ndash;45 anthology of drama plays by Oboler, often loaded with heavy-handed allegories.</p>",
"source": "<a href=\"https://archive.org/details/arch-obolers-plays-1945-04-26-3-the-house-i-live-in\" rel=\"external\">Internet Archive</a>",
"shows": [
	{
	"code": "B23",
	"heading": "B#23: <cite>Rocket from Manhattan</cite>",
	"blurb": "<p>The first moon landing discovers atomic test craters on the moon, and peace on Earth when they return.</p>",
	"notes": "suicidal intent",
	"file": "Rocket"
	}
]
},
{
"code": "ByM",
"heading": "<cite>Beyond Midnight</cite>",
"blurb": "<p>A 1968&ndash;70 anthology of supernatural dramas and horror stories adapted by the writer Michael McCabe and broadcast by the first commercial radio station in South Africa: Springbok Radio.</p>",
"source": "<a href=\"https://radioechoes.com/?page=series&genre=OTR-Thriller&series=Beyond%20Midnight\" rel=\"external\">Radio Echoes</a>",
"shows": [
	{
	"code": "11",
	"heading": "#11: <cite>No-Name Baby</cite>",
	"blurb": "<p>A woman believes her newborn baby is trying to kill her. Adapted from a story by Ray Bradbury.</p>",
	"notes": "asphyxiation, fall death, infanticide, paranoia",
	"file": "Baby"
	},
	{
	"code": "13",
	"heading": "#13: <cite>Lanceford House</cite><!--episode name on Radio Echoes is incorrect-->",
	"blurb": "<p>A writer seeking solitude moves to a country house and discovers a malign force obsessed with an ugly green vase. Adapted from a story by Dennis Roidt.</p>",
	"notes": "",
	"file": "Lanceford"
	},
	{
	"code": "18", // RL
	"heading": "#18: <cite>All At Sea</cite><!--episode name on Radio Echoes is an alternate: \"Upper Berth\"-->",
	"blurb": "<p>A sea traveller takes a cabin that seems to lead its inhabitants to suicide. Adapted from a story by F.&thinsp;Marion Crawford.</p>",
	"notes": "sanism",
	"file": "Sea"
	},
	{
	"code": "20",
	"heading": "#20: <cite>The Dream</cite>",
	"blurb": "<p>A physicist suffers recurring nightmares of being hunted by sinister warriors outside an ancient city. Adapted from a story by Basil Copper.</p>",
	"notes": "institutionalisation, racism",
	"file": "Dream"
	},
	{
	"code": "41",
	"heading": "#41: <cite>The Happy Return</cite>",
	"blurb": "<p>A man's fiancée is lost at sea; months later he finds the wreck, its calendar set to that very day. Adapted from a story by William Hope Hodgson.</p>",
	"notes": "",
	"file": "Return"
	}
]
},
{
"code": "BC",
"heading": "<cite>The Black Chapel</cite>",
"blurb": "<p>A 1937&ndash;39 horror anthology hosted and narrated by the insane organist of a derelict church (played by Ted Osbourne); only two episodes survive.</p>",
"source": "<a href=\"https://www.radioechoes.com/?page=series&genre=OTR-Thriller&series=The%20Black%20Chapel\" rel=\"external\">Radio Echoes</a>",
"shows": [
	{
	"code": "X",
	"heading": "#X: <cite>The Mahogany Coffin</cite>",
	"blurb": "<p>A gravedigger makes lavish preparations for his own burial&hellip; and an unlikely graveyard friend of his takes ghoulish revenge when a rival thwarts his plans.</p>",
	"notes": "entombment",
	"file": "Coffin"
	}
]
},
{
"code": "BkM",
"heading": "<cite>The Black Mass</cite>",
"blurb": "<p>A 1963&ndash;37 anthology of mostly gothic and cosmic horror stories adapted for radio, largely performed by Erik Bauersfeld.</p>",
"source": "<a href=\"https://www.kpfahistory.info/black_mass_home.html\" rel=\"external\"><abbr title=\"Pacifica Radio\">KPFA</abbr> History</a><!--the site of John Whiting, technical producer-->",
"shows": [
	{
	"code": "01", // RL
	"heading": "#1: <cite>All Hallows</cite>",
	"blurb": "<p>A traveller meets a verger in a derelict cathedral that the Christian believes is being invaded by evil. Adapted from a story by Walter de la Mare.</p>",
	"notes": "",
	"file": "Hallows"
	},
	{
	"code": "02",
	"heading": "#2: <cite>The Ash Tree</cite>",
	"blurb": "<p>A monstrous ash tree bears a terrible secret&mdash;when the &ldquo;witch&rdquo; who tends it is hanged, its &ldquo;curse&rdquo; falls on her accusers. Adapted from a story by M.&thinsp;R.&thinsp;James.</p>",
	"notes": "execution, spiders",
	"file": "Ash"
	},
	{
	"code": "04",
	"heading": "#4: <cite>Nightmare</cite>",
	"blurb": "<p>A paranoid man recounts how the helpful Dr. Fraser cured his illness, only to replace it with something much worse. Adapted from a story by Alan Wykes.</p>",
	"notes": "institutionalisation, public mockery",
	"file": "Nightmare"
	},
	{
	"code": "07",
	"heading": "#7: <cite>Oil of Dog</cite>",
	"blurb": "<p>A young man helps his father make <i>dog oil</i> and his mother dispose of <i>unwanted babies</i>&mdash;and combines his duties to disastrous effect. Adapted from a story by Ambrose Bierce.</p>",
	"notes": "animal abuse, infanticide, kidnapping, murder-suicide",
	"file": "Oil"
	},
	{
	"code": "08",
	"heading": "#8: <cite>The Death of Halpin Frayser</cite>",
	"blurb": "<p>Halpin Frayser wakes from dream and dies with the name Catherine LaRue on his lips. He was murdered. But how? Adapted from a story by Ambrose Bierce.</p>",
	"notes": "becoming lost, gore, domestic abuse, incest, strangulation, torture",
	"file": "Halpin"
	},
	{
	"code": "09",
	"heading": "#9: <cite>A Haunted House</cite>",
	"blurb": "<p>A house is haunted by the memory of the lovers who lived there. Adapted from a story by Virginia Woolf.</p>",
	"notes": "",
	"file": "House"
	},
	{
	"code": "11",
	"heading": "#11: <cite>A Predicament</cite> and <cite>The Tell-Tale Heart</cite>",
	"blurb": "<p>A murderer hears his victim's still-beating heart. A wealthy woman finds herself in a&hellip; predicament. Adapted from stories by Edgar Allen Poe.</p>",
	"notes": "animal death, gore, obsession",
	"file": "Predicament-Heart"
	},
	{
	"code": "12",
	"heading": "#12: <cite>Disillusionment</cite> and <cite>The Feeder</cite>",
	"blurb": "<p>A man lays out his philosophy of disenchantment. A patient's trapped in the void of his mind. Adapted from stories by Thomas Mann and Carl Linder.</p>",
	"notes": "coma, homophobia, life support, sexual harassment",
	"file": "Disillusionment-Feeder"
	},
	{
	"code": "14",
	"heading": "#14: <cite>The Imp of the Perverse</cite> and <cite><abbr class=\"unmodified-abbr\" title=\"Manuscript\">MS.</abbr> Found in a Bottle</cite>",
	"blurb": "<p>A prisoner explains his philosophy of temptation. A sea-traveller describes his strange and deadly final journey. Adapted from stories by Edgar Allen Poe.</p>",
	"notes": "being stranded at sea, darkness, drowning",
	"file": "Imp-MS"
	},
	{
	"code": "15",
	"heading": "#15: <cite>A Country Doctor</cite>",
	"blurb": "<p>A doctor makes a surreal journey through winter weather for a patient on his deathbed. Adapted from a story by Franz Kafka.</p>",
	"notes": "abduction, bite injury, description of open wound, insects, rape",
	"file": "Doctor"
	},
	{
	"code": "16",
	"heading": "#16: <cite>The Witch of the Willows</cite>",
	"blurb": "<p>A traveller seeking wonder and danger finds it in an eerie willow-marsh and its resident witch. Adapted from a story by Lord Dunsany.</p>",
	"notes": "",
	"file": "Willows"
	},
	{
	"code": "17",
	"heading": "#17: <cite>Atrophy</cite>",
	"blurb": "<p>A man's bodily strength withers from his toes to his head. Adapted from a story by John Anthony West.</p>",
	"notes": "food, gunshots (2:04)",
	"file": "Atrophy"
	},
	{
	"code": "20",
	"heading": "#20: <cite>Diary of a Madman</cite>",
	"blurb": "<p>The diary of a civil servant who slips into a world of delusion when his romantic desires are frustrated. Adapted from a story by Nikolai Gogol.</p>",
	"notes": "institutionalisation, obsession, sanism, social humiliation",
	"file": "Diary"
	},
	{
	"code": "21",
	"heading": "#21: <cite>The Outsider</cite>",
	"blurb": "<p>A lonely man in a deep, dark forest ponders what lies beyond&mdash;and one day makes his desperate escape. Adapted from a story by H.&thinsp;P.&thinsp;Lovecraft.</p>",
	"notes": "",
	"file": "Outsider"
	},
	{
	"code": "22",
	"heading": "#22: <cite>The Moonlit Road</cite>",
	"blurb": "<p>A man's mother is murdered; his father disappears. An outcast dreams of vile acts; a ghost returns to its beloved. Adapted from a story by Ambrose Bierce.</p>",
	"notes": "mental breakdown, strangulation, suspicion of cheating",
	"file": "Moonlit"
	},
	{
	"code": "X1",
	"heading": "X#1: <cite>The Haunter of the Dark</cite>, part 1",
	"blurb": "<p>A young writer seeks inspiration from an old cult's derelict church&mdash;he finds more than he bargained for. Adapted from a story by H.&thinsp;P.&thinsp;Lovecraft.</p>",
	"notes": "",
	"file": "Haunter-pt1"
	},
	{
	"code": "X2",
	"heading": "X#2: <cite>The Haunter of the Dark</cite>, part 2",
	"blurb": "<p>A young writer seeks inspiration from an old cult's derelict church&mdash;he finds more than he bargained for. Adapted from a story by H.&thinsp;P.&thinsp;Lovecraft.</p>",
	"notes": "",
	"file": "Haunter-pt2"
	},
	{
	"code": "X3",
	"heading": "X#3: <cite>Bartleby, the Scrivener</cite>",
	"blurb": "<p>A scribe decides to work no more&mdash;he would <q>prefer not to</q>. Adapted from a story by Herman Melville.</p>",
	"notes": "depression",
	"file": "Bartleby"
	},
	{
	"code": "X4",
	"heading": "X#4: <cite>The Judgement</cite>",
	"blurb": "<p>A father passes horrible judgement upon his son. Adapted from a story by Franz Kafka.</p>",
	"notes": "betrayal, emotional abuse, gaslighting, suicide",
	"file": "Judgement"
	},
	{
	"code": "X5",
	"heading": "X#5: <cite>Proof Positive</cite> and <cite>The Man in the Crowd</cite>",
	"blurb": "<p>An esoteric researcher shows <q>proof positive</q> of life after death. A people-watcher stalks an unreadable man. Adapted from stories by Graham Greene and Edgar Allen Poe.</p>",
	"notes": "",
	"file": "Proof-Crowd"
	},
	{
	"code": "X6",
	"heading": "X#6: <cite>Flies</cite>",
	"blurb": "<p>A starving tramp finds an empty house with a meal set at the table&hellip; is it a miracle, a trap, or something stranger? Adapted from a story by Anthony Vercoe.</p>",
	"notes": "fall death, gore, plague",
	"file": "Flies"
	}
]
},
{
"code": "CAS",
"heading": "<abbr class=\"unmodified-abbr\" title=\"Clark Ashton Smith\">CAS</abbr>iana",
"blurb": "<p>Collected readings of the weird fiction of Clark Ashton Smith.</p>",
"source": "<a href=\"http://www.eldritchdark.com/writings/spoken-word/\" rel=\"external\">The Eldritch Dark</a>",
"shows": [
	{
	"code": "1",
	"heading": "#1: <cite>The City of the Singing Flame</cite>, part 1",
	"blurb": "<p>A writer walks between worlds to a city where people sacrifice themselves to a vast, singing flame. Read by Mike Cothran.</p>",
	"notes": "self-destructive urges",
	"file": "Flame-pt1"
	},
	{
	"code": "2",
	"heading": "#2: <cite>The City of the Singing Flame</cite>, part 2",
	"blurb": "<p>A writer walks between worlds to a city where people sacrifice themselves to a vast, singing flame. Read by Mike Cothran.</p>",
	"notes": "self-destructive urges",
	"file": "Flame-pt2"
	},
	{
	"code": "3",
	"heading": "#3: <cite>The City of the Singing Flame</cite>, part 3",
	"blurb": "<p>A writer walks between worlds to a city where people sacrifice themselves to a vast, singing flame. Read by Mike Cothran.</p>",
	"notes": "self-destructive urges",
	"file": "Flame-pt3"
	},
	{
	"code": "4",
	"heading": "#4: <cite>The Door to Saturn</cite>, part 1",
	"blurb": "<p>An inquisitor and his occult quarry must unite to survive on an alien world. Read by Zilbethicus.</p>",
	"notes": "",
	"file": "Saturn-pt1"
	},
	{
	"code": "5",
	"heading": "#5: <cite>The Door to Saturn</cite>, part 2",
	"blurb": "<p>An inquisitor and his occult quarry must unite to survive on an alien world. Read by Zilbethicus.</p>",
	"notes": "alcohol",
	"file": "Saturn-pt2"
	},
	{
	"code": "6",
	"heading": "#6: <cite>The Maze of Maâl Dweb</cite>",
	"blurb": "<p>A hunter intrudes in the palace-maze of the sorcerer Maâl Dweb to rescue his beloved from the cruel lord. Read by Mike Cothran.</p>",
	"notes": "",
	"file": "Maze"
	},
	{
	"code": "7",
	"heading": "#7: <cite>The Dark Eidolon</cite>, part 1",
	"blurb": "<p>A necromancer takes exquisite revenge upon the ruler who wronged him. Read by Mike Cothran.</p>",
	"notes": "",
	"file": "Eidolon-pt1"
	},
	{
	"code": "8",
	"heading": "#8: <cite>The Dark Eidolon</cite>, part 2",
	"blurb": "<p>A necromancer takes exquisite revenge upon the ruler who wronged him. Read by Mike Cothran.</p>",
	"notes": "crush death, descriptions of gore, horse trampling, poisoning, possession",
	"file": "Eidolon-pt2"
	},
	{
	"code": "9",
	"heading": "poems",
	"blurb": "<p>Seven of Smith's poems, read by the author himself: <cite>High Surf</cite>, <cite>Malediction</cite>, <cite>Desert Dweller</cite>, <cite>Seeker</cite>, <cite>Moly</cite>, <cite>Nada</cite>, and <cite>Don Quixote on Market Street</cite>.</p>",
	"notes": "",
	"file": "Poems"
	}
]
},
{
"code": "CRW",
"heading": "<cite><abbr title=\"Columbia Broadcasting System\">CBS</abbr> Radio Workshop</cite>",
"blurb": "<p>A brief 1956&ndash;57 revival of the <cite>Columbia Workshop</cite> series' experimental radio tradition.</p>",
"source": "<a href=\"https://archive.org/details/OTRR_CBS_Radio_Workshop_Singles\" rel=\"external\">Internet Archive</a>",
"shows": [
	{
	"code": "66",
	"heading": "#66: <cite>Nightmare</cite>",
	"blurb": "<p>A tired, sick man has a surreal nightmare that shifts from memory to memory around him.</p>",
	"notes": "gaslighting, mob violence, parental death, seizure",
	"file": "Nightmare"
	}
]
},
{
"code": "CW",
"heading": "<cite>Columbia Workshop</cite>",
"blurb": "<p>A 1936&ndash;47 anthology of experimental radio plays organised by Irving Reis to push the narrative and technical boundaries of contemporary radio; succeeded by the <cite><abbr>CBS</abbr> Radio Workshop</cite>.</p>",
"source": "<a href=\"https://www.radioechoes.com/?page=series&genre=OTR-Drama&series=Columbia%20Workshop\" rel=\"external\">Radio Echoes</a>",
"shows": [
	{
	"code": "A031", // RL
	"heading": "A#31: <cite>Danse Macabre</cite>",
	"blurb": "<p>Death stalks the land figuratively, as plague brings a kingdom to its knees, and literally, as a lonely fiddler seeking someone to dance to his tune.</p>",
	"notes": "",
	"file": "Macabre"
	},
	{
	"code": "A034",
	"heading": "A#34: <cite>The Fall of the City</cite>",
	"blurb": "<p>Prophets, hierophants, and generals give grandiose speeches while the master of war descends on their &ldquo;free&rdquo; city. Featuring a young Orson Welles.</p>",
	"notes": "human sacrifice, mass panic",
	"file": "Fall"
	},
	{
	"code": "A107",
	"heading": "A#107: <cite>The Lighthouse Keepers</cite>",
	"blurb": "<p>A father and son meet tragedy when the younger begins acting strangely after an animal bite.</p>",
	"notes": "animal bite, bite injury, rabies, strangulation",
	"file": "Lighthouse"
	},
	{
	"code": "D27",
	"heading": "D#27: <cite>The City Wears a Slouch Hat</cite>",
	"blurb": "<p>An omniscient man slips from surreal scene to scene in a city under cacophonous downpour. Scored by John Cage, for household objects.</p>",
	"notes": "car crash, kidnapping, mugging",
	"file": "Slouch"
	}
]
},
{
"code": "DF",
"heading": "<cite>Dark Fantasy</cite>",
"blurb": "<p>A 1941&ndash;42 anthology of original horror stories and thrillers written by Scott Bishop, who also wrote for <cite>The Mysterious Traveler</cite>.</p>",
"source": "<a href=\"https://archive.org/details/OTRR_Dark_Fantasy_Singles\" rel=\"external\">Internet Archive</a>",
"shows": [
	{
	"code": "04",
	"heading": "#4: <cite>The Demon Tree</cite>",
	"blurb": "<p>Bored travellers at a holiday resort seek an infamous, murderous tree.</p>",
	"notes": "fall death, quicksand, strangulation",
	"file": "Tree"
	},
	{
	"code": "18",
	"heading": "#18: <cite>Pennsylvania Turnpike</cite>",
	"blurb": "<p>An ancient man waits at a turnpike gas station to help a fated traveller find the lost town of Pine Knob.</p>",
	"notes": "car crash, gunshot (22:41)",
	"file": "Turnpike"
	}
]
},
{
"code": "DX",
"heading": "<cite>Dimension X</cite>",
"blurb": "<p>(X X x x x&hellip;) A 1950&ndash;51 sci-fi anthology of originals and adaptations, mostly scripted by Ernest Kinoy and George Lefferts; the fore-runner to their later series <cite>X Minus One</cite>.</p>",
"source": "<a href=\"https://archive.org/details/OTRR_Dimension_X_Singles\" rel=\"external\">Internet Archive</a>",
"shows": [
	{
	"code": "02",
	"heading": "#2: <cite>With Folded Hands</cite>",
	"blurb": "<p>A seller of mechanical servants discovers a robotic conspiracy against human freedom. Adapted from a story by Jack Williamson.</p>",
	"notes": "alcohol, helplessness, lobotomy",
	"file": "Folded"
	},
	{
	"code": "11",
	"heading": "#11: <cite>There Will Come Soft Rains</cite> and <cite>Zero Hour</cite>",
	"blurb": "<p>An automated house goes through the motions after nuclear holocaust. Children play &ldquo;invasion&rdquo;, a game with deadly consequences. Adapted from stories by Ray Bradbury.</p>",
	"notes": "animal death, mentions of corporal punishment, fire, high-pitched sounds",
	"file": "Rains-Zero"
	},
	{
	"code": "14",
	"heading": "#14: <cite>Mars Is Heaven!</cite>",
	"blurb": "<p>The first humans on Mars find the alien world is a lot more familiar&mdash;a lot more like home&mdash;than anyone expected. Adapted from a story by Ray Bradbury.</p>",
	"notes": "alcohol, betrayal, food adverts, gunshots (8:50&ndash;8:53), Holocaust mention, uncanny valley",
	"file": "Heaven"
	},
	{
	"code": "15",
	"heading": "#15: <cite>The Man in the Moon</cite>",
	"blurb": "<p>The <abbr class=\"unmodified-abbr\">US</abbr> Missing Persons Bureau receives calls for help&mdash;beamed straight from the moon.</p>",
	"notes": "abduction, betrayal, food adverts, gunshots (20:00), sanism",
	"file": "Moon"
	},
	{
	"code": "17",
	"heading": "#17: <cite>The Potters of Firsk</cite>",
	"blurb": "<p>A space colonist learns the secret of planet Firsk's exquisite pottery, made with the aid of its ancestors. Adapted from a story by Jack Vance.</p>",
	"notes": "food adverts, gunshot (12:58), kidnapping, racism",
	"file": "Potters"
	},
	{
	"code": "21",
	"heading": "#21: <cite>The Parade</cite>",
	"blurb": "<p>A wealthy client hires an ad firm to announce an alien invasion&mdash;ending in a Martian military parade.</p>",
	"notes": "food adverts, gunshot? (26:54), panic",
	"file": "Parade"
	},
	{
	"code": "25",
	"heading": "#25: <cite>Dr. Grimshaw's Sanitorium</cite>",
	"blurb": "<p>A patient vanishes from a sanitarium&mdash;the doctors say he's dead, but a <abbr class=\"unmodified-abbr\" title=\"private investigator\">PI</abbr>'s not so sure. Adapted from a story by Fletcher Pratt.</p>",
	"notes": "ableism, alcoholism, betrayal, animal attack, car crash, human experiments, injection, mention of suicide, Naziism, non-consensual surgery, sanism",
	"file": "Sanitorium"
	},
	{
	"code": "26",
	"heading": "#26: <cite>And the Moon be Still as Bright</cite>",
	"blurb": "<p>The final human expedition to Mars discovers why the red planet is dead; one man abandons humanity in turn. Adapted from a story by Ray Bradbury.</p>",
	"notes": "alcohol, genocide, gunshots (17:20, 17:54, 19:46, 20:06, 25:47, 26:50)",
	"file": "Bright"
	},
	{
	"code": "28",
	"heading": "#28: <cite>The Professor Was a Thief</cite>",
	"blurb": "<p>An eccentric professor causes chaos in New York by stealing landmarks, city blocks, skyscrapers. Adapted from a story by L.&thinsp;Ron Hubbard.</p>",
	"notes": "alcohol",
	"file": "Professor"
	},
	{
	"code": "31",
	"heading": "#31: <cite>Universe</cite>",
	"blurb": "<p>The warring peoples of a universe 25 kilometers wide and 100 levels deep must unite to avert disaster. Adapted from a story by Robert A.&thinsp;Heinlein.</p>",
	"notes": "ableism, cult behaviour, gunshots (8:41&ndash;8:42, 26:01, 26:07&ndash;26:10, 27:19, lynching)",
	"file": "Universe"
	},
	{
	"code": "36",
	"heading": "#36: <cite>Nightmare</cite>",
	"blurb": "<p>All technology, from computers to door handles, conspires against its human masters. Adapted from a poem by Stephen Vincent Benét.</p>",
	"notes": "industrial deaths, institutionalisation, paranoia, traffic deaths",
	"file": "Nightmare"
	},
	{
	"code": "46",
	"heading": "#46: <cite>Marionettes, Inc.</cite>",
	"blurb": "<p>Two men chat about their marital troubles and one suggests a&hellip; mechanical substitute. Adapted from a story by Ray Bradbury.</p>",
	"notes": "alcohol, doppelganger, entombment, gunshots (22:33), toxic relationships",
	"file": "Marionettes"
	},
	{
	"code": "48",
	"heading": "#48: <cite>Kaleidoscope</cite>",
	"blurb": "<p>A spaceship is destroyed, leaving the survivors to drift apart with nothing to do but talk, think, and die. Adapted from a story by Ray Bradbury.</p>",
	"notes": "isolation, parental negligence, suicide",
	"file": "Kaleidoscope"
	},
	{
	"code": "50",
	"heading": "#50: <cite>Nightfall</cite>",
	"blurb": "<p>Astronomers on a world ringed by six suns declare a doomsday prophecy: night will fall and cities will burn. Adapted from a story by Isaac Asimov.</p>",
	"notes": "alcohol, cult belief, mental breakdown",
	"file": "Nightfall"
	}
]
},
{
"code": "Esc",
"heading": "<cite>Escape</cite>",
"blurb": "<p>A 1947&ndash;54 anthology of escapist radio plays that shared its talent with the longer-running <cite>Suspense</cite>, and more often delved into the supernatural or science-fiction.</p>",
"source": "<a href=\"https://archive.org/details/OTRR_Escape_Singles\" rel=\"external\">Internet Archive</a>",
"shows": [
	{
	"code": "006",
	"heading": "#6: <cite>The Ring of Thoth</cite>",
	"blurb": "<p>An Egyptologist has a chance encounter with the subjects of an ancient tragedy. Adapted from a story by Arthur Conan Doyle, starring Jack Webb.</p>",
	"notes": "plague",
	"file": "Thoth"
	},
	{
	"code": "015",
	"heading": "#15: <cite>Casting the Runes</cite>",
	"blurb": "<p>A scholar pens a scathing review of a crackpot's work, inciting arcane revenge. Adapted from a story by M.&thinsp;R.&thinsp;James.</p>",
	"notes": "food poisoning, stalking",
	"file": "Runes"
	},
	{
	"code": "052",
	"heading": "#52: <cite>A Dream of Armageddon</cite>",
	"blurb": "<p>A man dreams of a world rolling inevitably towards annihilation. Adapted from a story by H.&thinsp;G.&thinsp;Wells.</p>",
	"notes": "being eaten while conscious, explosions (13:56&ndash;14:08), gunshots (14:43, 19:48&ndash;50, 22:47&ndash;50), poison gas, stab death, starvation",
	"file": "Armageddon"
	},
	{
	"code": "053",
	"heading": "#53: <cite>Evening Primrose</cite>",
	"blurb": "<p>A poet rejects society to live unseen in a department store&mdash;but he's not the first. Adapted from a story by John Collier, starring William Conrad.</p>",
	"notes": "",
	"file": "Primrose"
	},
	{
	"code": "117",
	"heading": "#117: <cite>Blood Bath</cite>",
	"blurb": "<p>Five men find a trillion-dollar deposit of Amazonian uranium, but greed gets the better of them. Starring Vincent Price.</p>",
	"notes": "betrayal, malaria, eaten by piranhas",
	"file": "Bath"
	},
	{
	"code": "209",
	"heading": "#209: <cite>The Scarlet Plague</cite>",
	"blurb": "<p>A pandemic rages across the world, driving the survivors to violence and escape. Adapted from a story by Jack London.</p>",
	"notes": "alcohol, animal slaughter, eating raw meat, gunshot (16:58), survivor's guilt",
	"file": "Scarlet"
	}
]
},
{
"code": "FOT",
"heading": "<cite>Fifty-One Tales</cite>",
"blurb": "<p>A collection of fifty-one short tales of myth and fantasy by Lord Dunsany, first published in 1915 and read by Rosslyn Carlyle for LibriVox.</p>",
"source": "<a href=\"https://librivox.org/fifty-one-tales-by-lord-dunsany\" rel=\"external\">LibriVox</a>",
"shows": [
	{
	"code": "1",
	"heading": "part 1",
	"blurb": "<p>From <cite>The Assignation</cite> to <cite>The Unpasturable Fields</cite>.</p>",
	"notes": "fall death, poisoning, suicide",
	"file": "starts-Assignation"
	},
	{
	"code": "2",
	"heading": "part 2",
	"blurb": "<p>From <cite>The Worm and the Angel</cite> to <cite>Spring in Town</cite>.</p>",
	"notes": "",
	"file": "starts-Angel"
	},
	{
	"code": "3",
	"heading": "part 3",
	"blurb": "<p>From <cite>How the Enemy Came to Thlunrana</cite> to <cite>The Reward</cite>.</p>",
	"notes": "",
	"file": "starts-Enemy"
	},
	{
	"code": "4",
	"heading": "part 4",
	"blurb": "<p>From <cite>The Trouble in Leafy Green Street</cite> to <cite>The Tomb of Pan</cite>.</p>",
	"notes": "animal sacrifice, lynching",
	"file": "starts-Trouble"
	}
]
},
{
"code": "GGP",
"heading": "<cite>The Great God Pan</cite>",
"blurb": "<p>A doctor lets the universe into a woman's brain. A village girl lures children into chaos. A seductress drives high society men to suicide. Written by Arthur Machen, first published in 1894, and read by Ethan Rampton for LibriVox.</p>",
"source": "<a href=\"https://librivox.org/the-great-god-pan-by-arthur-machen\" rel=\"external\">LibriVox</a>",
"shows": [
	{
	"code": "1",
	"heading": "part 1",
	"blurb": "<p>Chapters 1 and 2: <cite>The Experiment</cite> and <cite>Mr. Clarke's Memoirs</cite>.</p>",
	"notes": "ableism, brain surgery, child death?, child endangerment",
	"file": "Experiment-Memoirs"
	},
	{
	"code": "2",
	"heading": "part 2",
	"blurb": "<p>Chapters 3 and 4: <cite>The City of Resurrections</cite> and <cite>The Discovery in Paul Street</cite>.</p>",
	"notes": "",
	"file": "City-Street"
	},
	{
	"code": "3",
	"heading": "part 3",
	"blurb": "<p>Chapters 5 and 6: <cite>The Letter of Advice</cite> and <cite>The Suicides</cite>.</p>",
	"notes": "",
	"file": "Letter-Suicides"
	},
	{
	"code": "4",
	"heading": "part 4",
	"blurb": "<p>Chapters 7 and 8: <cite>The Encounter in Soho</cite> and <cite>The Fragments</cite>.</p>",
	"notes": "body horror",
	"file": "Soho-Fragments"
	}
]
},
{
"code": "HF",
"heading": "<cite>The Hall of Fantasy</cite>",
"blurb": "<p>A series of supernatural horror stories originally broadcast in Utah, later nationally syndicated in 1952&ndash;53. The series was written and directed by Richard Thorne.</p>",
"source": "<a href=\"https://archive.org/details/470213ThePerfectScript\" rel=\"external\">Internet Archive</a>",
"shows": [
	{
	"code": "A21",
	"heading": "A#21: <cite>The Judge's House</cite>",
	"blurb": "<p>An exam student rents a malignant old house and comes face to face with the hanging judge who once lived there. Adapted from a story by Bram Stoker.</p>",
	"notes": "animal swarm",
	"file": "Judge"
	},
	{
	"code": "B1",
	"heading": "B#1: <cite>The Legend of Drago</cite>",
	"blurb": "<p>Tourists travelling through Europe in August 1939 encounter an immortal, war-loving evil in a castle where no living thing will tread.</p>",
	"notes": "",
	"file": "Drago"
	},
	{
	"code": "B3",
	"heading": "B#3: <cite>The Shadow People</cite>",
	"blurb": "<p>A young woman is targeted by murderous living shadows that extinguish her family one by one.</p>",
	"notes": "",
	"file": "Shadow"
	},
	{
	"code": "C06",
	"heading": "C#6: <cite>The Dance of the Devil Dolls</cite>",
	"blurb": "<p>Two travellers stumble into a web of conspiracy, black magic, and murder by living dolls.</p>",
	"notes": "gunshots (21:00)",
	"file": "Dolls"
	},
	{
	"code": "C10",
	"heading": "C#10: <cite>The Masks of Ashor</cite>",
	"blurb": "<p>A couple receive a curio from their world-travelling uncle&mdash;two golden masks belonging to an ancient devil of death.</p>",
	"notes": "animal attack",
	"file": "Masks"
	},
	{
	"code": "C12",
	"heading": "C#12: <cite>The Night the Fog Came</cite>",
	"blurb": "<p>Scientists discover microbes that spread by fog and drown anyone they touch&mdash;and the fog is spreading in a wave of death.</p>",
	"notes": "",
	"file": "Fog"
	},
	{
	"code": "C20",
	"heading": "C#20: <cite>The Crawling Thing</cite>",
	"blurb": "<p>Scientists mutate a spider into a sinister monster that's much more dangerous than they imagine.</p>",
	"notes": "hypnosis",
	"file": "Crawling"
	},
	{
	"code": "C27",
	"heading": "C#27: <cite>The Man in Black</cite>",
	"blurb": "<p>Two friends catch the eye of the nocturnal evil, the &ldquo;man in black&rdquo;.</p>",
	"notes": "",
	"file": "Black"
	},
	{
	"code": "C32",
	"heading": "C#32: <cite>The Man From the Second Earth</cite>",
	"blurb": "<p>An astronomer reveals Earth to an alien race that sends an agent to take humanity's measure.</p>",
	"notes": "abduction, betrayal, body horror, fire death",
	"file": "Second"
	},
	{
	"code": "C43",
	"heading": "C#43: <cite>He Who Follows Me</cite>",
	"blurb": "<p>A couple disturb a sleeping evil in the grounds of an abandoned mansion&mdash;the &ldquo;Death That Walks&rdquo;.</p>",
	"notes": "stalking",
	"file": "Follows"
	}
]
},
{
"code": "KY",
"heading": "<cite>The King in Yellow</cite>",
"blurb": "<p>A collection of eldritch horror stories from this classic collection, all revolving around an eerie symbol, a bizarre play, a mysterious abomination, and lost Carcosa. Written by Robert W.&thinsp;Chambers, first published in 1895, and read by Eva Staes for LibriVox.</p>",
"source": "<a href=\"https://librivox.org/king-in-yellow-version-2-by-robert-w-chambers\" rel=\"external\">LibriVox</a>",
"shows": [
	{
	"code": "0",
	"heading": "poems",
	"blurb": "<p>Two poems from the collection: the dedication (a fragment of the play) and <cite>The Prophet's Paradise</cite>.</p>",
	"notes": "",
	"file": "Poems"
	},
	{
	"code": "1",
	"heading": "1: <cite>The Repairer of Reputations</cite>, part 1",
	"blurb": "<p>A man losing touch with reality plots to overthrow the aristocratic-fascist government and declare himself supreme leader.</p>",
	"notes": "hallucination?, institutionalisation, mention of suicide",
	"file": "Repairer-pt1"
	},
	{
	"code": "2",
	"heading": "1: <cite>The Repairer of Reputations</cite>, part 2",
	"blurb": "<p>A man losing touch with reality plots to overthrow the aristocratic-fascist government and declare himself supreme leader.</p>",
	"notes": "ableism, hallucination, suicide",
	"file": "Repairer-pt2"
	},
	{
	"code": "3",
	"heading": "1: <cite>The Repairer of Reputations</cite>, part 3",
	"blurb": "<p>A man losing touch with reality plots to overthrow the aristocratic-fascist government and declare himself supreme leader.</p>",
	"notes": "animal attack, hallucination?, institutionalisation",
	"file": "Repairer-pt3"
	},
	{
	"code": "4",
	"heading": "2: <cite>The Mask</cite>",
	"blurb": "<p>Bohemians experiment with drugs, art, and a concoction that turns anything into solid marble.</p>",
	"notes": "suicide",
	"file": "Mask"
	},
	{
	"code": "5",
	"heading": "3: <cite>The Court of the Dragon</cite>",
	"blurb": "<p>After reading the banned play <cite>The King in Yellow</cite>, a man is haunted by a sinister organist and visions of the living god.</p>",
	"notes": "",
	"file": "Court"
	},
	{
	"code": "6",
	"heading": "4: <cite>The Yellow Sign</cite>",
	"blurb": "<p>A painter and his model encounter an abomination after reading the banned play <cite>The King in Yellow</cite>.</p>",
	"notes": "",
	"file": "Sign"
	}
]
},
{
"code": "LV",
"heading": "LibriVox selection",
"blurb": "<p>LibriVox is a catalogue of public domain audiobook readings, including a selection of weird fiction and horror classics from decades and centuries ago.</p>",
"source": "<a href=\"https://librivox.org\" rel=\"external\">LibriVox</a>",
"shows": [
	{
	"code": "01",
	"heading": "#1: <cite>The Nameless City</cite>",
	"blurb": "<p>An archaeologist journeys to, and within, an ancient, nameless city not built to human proportions. Written by H.&thinsp;P.&thinsp;Lovecraft, read by Dean Delp.</p>",
	"notes": "claustrophobia, darkness",
	"file": "Nameless"
	},
	{
	"code": "02",
	"heading": "#2: <cite>An Occurrence at Owl Creek Bridge</cite>",
	"blurb": "<p>A Confederate slave owner and attempted saboteur has a miraculous, harrowing escape from execution. Written by Ambrose Bierce, read by Elise Sauer.</p>",
	"notes": "entrapment, facial wounds, hallucination?, hanging, near-drowning",
	"file": "Occurrence"
	},
	{
	"code": "03",
	"heading": "#3: <cite>The White Ship</cite>",
	"blurb": "<p>A lighthouse keeper fantasises of visiting wondrous islands on a white ship&mdash;but his daydream may be more real than he thinks. Written by H.&thinsp;P.&thinsp;Lovecraft, read by D.&thinsp;E.&thinsp;Wittkower.</p>",
	"notes": "animal death, disease, near-drowning, shipwreck",
	"file": "Ship"
	},
	{
	"code": "04",
	"heading": "#4: <cite>The Horror of the Heights</cite>",
	"blurb": "<p>An aviator investigates the unsolved deaths of pilots who broke the altitude record&mdash;and discovers an ecosystem in the sky. Written by Arthur Conan Doyle, read by Mike Harris.</p>",
	"notes": "being hunted, decapitation, isolation, jellyfish",
	"file": "Heights"
	},
	{
	"code": "05",
	"heading": "#5: Gods and Abominations (short works by Lovecraft)",
	"blurb": "<p>Three short pieces by H.&thinsp;P.&thinsp;Lovecraft: <cite>What the Moon Brings</cite> (read by Dan Gurzynski), <cite>Dagon</cite> (read by Selim Jamil), and <cite>Nyarlathotep</cite> (read by Tom Hackett).</p><!--\"Nyarlathotep\" reader's name on LibriVox site (Tom Hackett) is different to the one given in the file (Peter Bianzo(?))-->",
	"notes": "being stranded at sea, drowning, drugs, isolation, near-drowning, nightmares, racism, suicide planning, worms",
	"file": "Moon-Dagon-Nyarlathotep"
	},
	{
	"code": "07",
	"heading": "#7: <cite>The Night Wire</cite>",
	"blurb": "<p>Graveyard shift telegram operators receive reports from an unknown town beset by mist and monsters. Written by H.&thinsp;F.&thinsp;Arnold, read by Dan Gurzynski.</p>",
	"notes": "",
	"file": "Wire"
	},
	{
	"code": "08",
	"heading": "#8: <cite><abbr class=\"unmodified-abbr\" title=\"To Be Or Not To Be\">2 B R 0 2 B</abbr></cite>",
	"blurb": "<p>In a future of immortality and severe population control, a father must find three people willing to die so his newborn triplets can be permitted to live. Written by Kurt Vonnegut, read by Alex Clarke.</p>",
	"notes": "gun death, suicide, suicide ideation",
	"file": "Naught"
	},
	{
	"code": "09",
	"heading": "#9: <cite>The Lord of Cities</cite>",
	"blurb": "<p>A human traveller, the road and the river, and the earth itself ruminate on life, beauty, and purpose. Written by Lord Dunsany, read by Ed Humpal.</p>",
	"notes": "",
	"file": "Cities"
	},
	{
	"code": "10",
	"heading": "#10: Gothic Origins (short works by Poe)",
	"blurb": "<p>Two short pieces by Edgar Allen Poe: <cite>The Masque of the Red Death</cite> (read by Elise Dee) and <cite>The Cask of Amontillado</cite> (read by &ldquo;Caveat&rdquo;).</p>",
	"notes": "betrayal, darkness, entombment",
	"file": "Masque-Amontillado"
	},
	{
	"code": "12",
	"heading": "#12: Things That Talk (short works by Dunsany)",
	"blurb": "<p>Three short pieces by Lord Dunsany: <cite>Blagdaross</cite> (read by Michele Fry), <cite>The Unhappy Body</cite> (read by Andrew Gaunce), and <cite>The Madness of Andlesprutz</cite> (read by Michele Fry).</p>",
	"notes": "abandonment, overwork",
	"file": "Blagdaross-Unhappy-Madness"
	},
	{
	"code": "13",
	"heading": "#13: <cite>The Monkey's Paw</cite>",
	"blurb": "<p>A mummified monkey's paw grants three wishes&mdash;with dread consequences. Written by W.&thinsp;W.&thinsp;Jacobs, read by Travis Baldree.</p>",
	"notes": "",
	"file": "Paw"
	},
	{
	"code": "15",
	"heading": "#15: <cite>Mysterious Disappearances</cite>",
	"blurb": "<p>Three accounts of unexplained disappearances. Written by Ambrose Bierce, read by Piotr Nater.</p>",
	"notes": "",
	"file": "Disappearances"
	},
	{
	"code": "16",
	"heading": "#16: <cite>Lost Hearts</cite>",
	"blurb": "<p>An orphan is taken in by an immortality-seeking alchemist, but is disturbed by child-spectres at his estate. Written by M.&thinsp;R.&thinsp;James, read by J.&thinsp;M.&thinsp;H.&thinsp;Powell.</p>",
	"notes": "child death, disembowelment",
	"file": "Hearts"
	},
	{
	"code": "17",
	"heading": "#17: <cite>What Was It?</cite>",
	"blurb": "<p>Lodgers at a supposedly-haunted house discover an invisible supernumerary resident. Written by Fitz-James O'Brien, read by Rafe Ball.</p>",
	"notes": "being trapped, starvation, strangulation",
	"file": "What"
	},
	{
	"code": "18",
	"heading": "#18: Dreams and Nightmares (poems)",
	"blurb": "<p>Collected poems on dreams and nightmares. Written by Seigfried Sassoon, Samuel Taylor Coleridge, Helen Hunt Jackson, Clark Ashton Smith, William Danby, and John James Piatt; read by Nemo, Algy Pug, Newgatenovelist, and Colleen McMahon.</p>",
	"notes": "",
	"file": "Dreams-and-Nightmares"
	},
	{
	"code": "19",
	"heading": "#19: <cite>Carcassonne</cite>",
	"blurb": "<p>A young king and his warriors attempt to conquer the unconquerable. Written by Lord Dunsany, read by Daniel Davison.</p>",
	"notes": "",
	"file": "Carcassonne"
	},
	{
	"code": "20",
	"heading": "#20: <cite>The Facts in the Case of <abbr class=\"unmodified-abbr\" title=\"Monsieur\">M.</abbr> Valdemar</cite>",
	"blurb": "<p>A mesmerist preserves a man beyond death. Written by Edgar Allen Poe, read by Tony Scheinman.</p>",
	"notes": "extensive descriptions of gore, suicide",
	"file": "Facts"
	},
	{
	"code": "21",
	"heading": "#21: <cite>Imprisoned with the Pharoahs</cite>, part 1",
	"blurb": "<p>A &ldquo;true&rdquo; story of escape artist Harry Houdini's dark encounter under the Sphinx of Giza. Written by H.&thinsp;P.&thinsp;Lovecraft with Houdini, read by Ben Tucker.</p>",
	"notes": "betrayal, darkness, kidnapping, racism",
	"file": "Pharoahs-pt1"
	},
	{
	"code": "22",
	"heading": "#22: <cite>Imprisoned with the Pharoahs</cite>, part 2",
	"blurb": "<p>A &ldquo;true&rdquo; story of escape artist Harry Houdini's dark encounter under the Sphinx of Giza. Written by H.&thinsp;P.&thinsp;Lovecraft with Houdini, read by Ben Tucker.</p>",
	"notes": "betrayal, darkness, kidnapping, racism",
	"file": "Pharoahs-pt2"
	},
	{
	"code": "23",
	"heading": "#23: <cite>A Hunger Artist</cite>",
	"blurb": "<p>The life of a performer who starves themself for the crowd. Written by Franz Kafka, read by Cori Samuel.</p>",
	"notes": "",
	"file": "Hunger"
	},
	{
	"code": "24",
	"heading": "#24: <cite>Beyond the Door</cite>",
	"blurb": "<p>A man discovers a haunting secret in his deceased uncle's diary&hellip; and under his house. Written by Paul Suter, read by Chris Burrage.</p>",
	"notes": "darkness, paralysis, suicide",
	"file": "Beyond"
	},
	{
	"code": "25",
	"heading": "#25: <cite>The Stroller</cite>",
	"blurb": "<p>A bickering couple have a near-deadly doppelganger encounter. Written by Margaret St. Clair, read by quartertone.</p>",
	"notes": "betrayal",
	"file": "Stroller"
	},
	{
	"code": "27",
	"heading": "#27: Tales of Cities",
	"blurb": "<p><cite>The Idle City</cite> (written by Lord Dunsany, read by Daniel Davison) and <cite>The City of My Dreams</cite> (written by Theodore Dreiser, read by Phil Schempf).</p>",
	"notes": "",
	"file": "Idle-Dreams"
	},
	{
	"code": "30",
	"heading": "#30: <cite>The Doom That Came to Sarnath</cite>",
	"blurb": "<p>A young city rises by exterminating its neighbours, until doom descends upon it in turn. Written by H.&thinsp;P.&thinsp;Lovecraft, read by Glen Hallstrom.</p>",
	"notes": "",
	"file": "Sarnath"
	},
	{
	"code": "31",
	"heading": "#31: <cite>The Valley of Spiders</cite>",
	"blurb": "<p>A lord and his servants pursue a girl into a most deadly valley. Written by H.&thinsp;G.&thinsp;Wells, read by Robert Dixon.</p>",
	"notes": "being trapped, betrayal, foot injury",
	"file": "Valley"
	},
	{
	"code": "32",
	"heading": "#32: <cite>The Crawling Chaos</cite>",
	"blurb": "<p>A man in an opium abyss has a beautiful vision of apocalypse. Written by H.&thinsp;P.&thinsp;Lovecraft, read by D.&thinsp;E&thinsp;Wittkower.</p>",
	"notes": "",
	"file": "Chaos"
	},
	{
	"code": "33",
	"heading": "#33: <cite>Caterpillars</cite>",
	"blurb": "<p>Monstrous caterpillars haunt the visitor to an isolated villa&mdash;in dream, then reality. Written by E.&thinsp;F.&thinsp;Benson, read by Andy Minter.</p>",
	"notes": "cancer",
	"file": "Caterpillars"
	},
	{
	"code": "34",
	"heading": "#34: <cite>Fog</cite>",
	"blurb": "<p>A sickly boy and ethereal girl long for each other over the gulf of fog, sea, and time. Written by Dana Burnet, read by Colleen McMahon.</p>",
	"notes": "",
	"file": "Fog"
	},
	{
	"code": "35",
	"heading": "#35: <cite>Ancient Lights</cite>",
	"blurb": "<p>A surveyor hired to help clear a fey wood gets lost in illusion after walking between the trees. Written by Algernon Blackwood, read by Samanem.</p>",
	"notes": "",
	"file": "Ancient"
	},
	{
	"code": "36",
	"heading": "#36: <cite>The Kingdom of the Worm</cite>",
	"blurb": "<p>A travelling knight offends the king of worms and is imprisoned in its kingdom. Written by Clark Ashton Smith, read by Ben Tucker.</p>",
	"notes": "claustrophobia, darkness",
	"file": "Kingdom"
	},
	{
	"code": "37",
	"heading": "#37: <cite>The Man Who Found Out</cite>",
	"blurb": "<p>Two academics discover the horrifying truth of the world&mdash;knowledge that kills their souls while their bodies still walk. Written by Algernon Blackwood, read by Mike Pelton.</p>",
	"notes": "depression, suicide ideation",
	"file": "Found"
	},
	{
	"code": "38",
	"heading": "#38: <cite>The Primal City</cite>",
	"blurb": "<p>An expedition to an ancient mountain city finds its denizens gone, but its sentinels on alert. Written by Clark Ashton Smith, read by Ben Tucker.</p>",
	"notes": "",
	"file": "Primal"
	},
	{
	"code": "39",
	"heading": "#39: Forgotten Gods (short works by Dunsany)",
	"blurb": "<p>Three short pieces by Lord Dunsany, read by Sandra Cullum: <cite>The Gift of the Gods</cite>, <cite>An Archive of the Older Mysteries</cite>, and <cite>How the Office of Postman Fell Vacant in Otford-under-the-Wold</cite>.</p>",
	"notes": "",
	"file": "Gift-Archive-Postman"
	},
	{
	"code": "40",
	"heading": "#40: Hauntings and Vanishings (short works by Bierce)",
	"blurb": "<p>Three short pieces by Ambrose Bierce: <cite>The Spook House</cite> (read by Paul Sigel), <cite>A Cold Greeting</cite> (read by Steve Karafit), and <cite>An Inhabitant of Carcosa</cite> (read by G.&thinsp;C.&thinsp;Fournier).</p>",
	"notes": "entombment",
	"file": "Spook-Greeting-Carcosa"
	},
	{
	"code": "41",
	"heading": "#41: <cite>The Music of Erich Zann</cite>",
	"blurb": "<p>A musician plays nightly eldritch harmonies for the void beyond his window. Written by H.&thinsp;P.&thinsp;Lovecraft, read by Cameron Halket.</p>",
	"notes": "ableism",
	"file": "Zann"
	},
	{
	"code": "42",
	"heading": "#42: Ancient Humanities (short works by Lovecraft)",
	"blurb": "<p>Two short pieces by H.&thinsp;P.&thinsp;Lovecraft: <cite>Polaris</cite> (read by jpontoli) and <cite>The Outer Gods</cite> (read by Peter Yearsley).</p>",
	"notes": "racism",
	"file": "Polaris-Outer"
	},
	{
	"code": "43",
	"heading": "#43: <cite>Novel of the White Powder</cite>",
	"blurb": "<p>An excerpt from the occult horror novel <cite>The Three Impostors; or, The Transmutations</cite>: a mourner relates how a medicine transfigured her brother. Written by Arthur Machen, read by Tony Oliva.</p>",
	"notes": "body horror",
	"file": "Powder"
	},
	{
	"code": "44",
	"heading": "#44: <cite>The Fall of Babbulkund</cite>",
	"blurb": "<p>Babbulkund, City of Marvel, city of wonders, city on travellers' lips&mdash;Babbulkund has fallen, decayed, died. Written by Lord Dunsany, read by Alex Clarke.</p>",
	"notes": "",
	"file": "Babbulkund"
	},
	{
	"code": "45",
	"heading": "#45: Power and Spectacle (short works by Kafka)",
	"blurb": "<p>Three short pieces by Franz Kafka: <cite>Before the Law</cite> (read by Availle), <cite>Up in the Gallery</cite> (read by Adam Whybray), and <cite>An Imperial Message</cite> (read by SBE Iyyerwal).</p>",
	"notes": "",
	"file": "Law-Gallery-Message"
	},
	{
	"code": "46",
	"heading": "#46: <cite>The Sword of Welleran</cite>",
	"blurb": "<p>The city of Merimna mourns its heroes, heedless of the foes approaching now its guardians are dead. Written by Lord Dunsany, read by Ed Humpal.</p>",
	"notes": "",
	"file": "Welleran"
	},
	{
	"code": "47",
	"heading": "#47: Great and Little Ones (short works by Dunsany)",
	"blurb": "<p>Three short pieces by Lord Dunsany: <cite>The Whirlpool</cite> (read by James Koss), <cite>The Hurricane</cite> (read by Rosslyn Carlyle), and <cite>On the Dry Land</cite> (read by Rosslyn Carlyle).</p>",
	"notes": "",
	"file": "Whirlpool-Hurricane-Dry"
	},
	{
	"code": "48",
	"heading": "#48: <cite>Gaffer Death</cite>",
	"blurb": "<p>The German folktale telling of a doctor whose godfather is Death himself. Compiled by Charles John Tibbitts, read by Craig Campbell.</p>",
	"notes": "",
	"file": "Gaffer"
	}
]
},
{
"code": "LO",
"heading": "<cite>Lights Out</cite>",
"blurb": "<p>One of the earliest radio horror shows, started by Wyllis Cooper in 1934, later headed by Arch Oboler until 1947. Often more camp than scary, by modern standards.</p>",
"source": "<a href=\"https://archive.org/details/LightsOutoldTimeRadio\" rel=\"external\">Internet Archive</a>",
"shows": [
	{
	"code": "A009",
	"heading": "A#9: <cite>Money, Money, Money</cite>",
	"blurb": "<p>A sailor murders his colleague for a winning lottery ticket and takes an offshore salvage-diving job to escape the law&mdash;but he can't escape revenge.</p>",
	"notes": "narcosis",
	"file": "Money"
	},
	{
	"code": "A040-A072-C09",
	"heading": "A#40, A#72, and C#9: <cite>Lights Out</cite> fragments",
	"blurb": "<p>A trio of short <cite>Lights Out</cite> episodes: <cite>Chicken Heart</cite>, <cite>The Dark</cite>, and <cite>The Day the Sun Exploded</cite>.</p>",
	"notes": "body horror, plane crash, racism",
	"file": "Chicken-Dark-Exploded"
	},
	{
	"code": "A051",
	"heading": "A#51: <cite>Until Dead</cite>",
	"blurb": "<p>A man falsely accused of killing his wife sets out for revenge&mdash;even after imprisonment, after death.</p>",
	"notes": "claustrophobia, drowning, stab death, suicide",
	"file": "Until"
	},
	{
	"code": "A071",
	"heading": "A#71: <cite>Christmas Story</cite>",
	"blurb": "<p>Three soldiers meet in a train on Christmas Eve, 1918, but they feel like they've met before&hellip;</p>",
	"notes": "",
	"file": "Christmas"
	},
	{
	"code": "A076",
	"heading": "A#76: <cite>Oxychloride X</cite>",
	"blurb": "<p>A put-upon and rejected chemistry student makes a miracle to spite his tormentors.</p>",
	"notes": "falling death",
	"file": "Oxychloride"
	},
	{
	"code": "A086",
	"heading": "A#86: <cite>Cat Wife</cite>",
	"blurb": "<p>A man accidentally curses his selfish wife to become a cat. Starring Boris Karloff.</p>",
	"notes": "",
	"file": "Cat"
	},
	{
	"code": "B02",
	"heading": "B#2: <cite>Revolt of the Worms</cite>",
	"blurb": "<p>A chemist carelessly disposes of a growth formula, with disastrous consequences.</p>",
	"notes": "crushing death",
	"file": "Worms"
	},
	{
	"code": "B07", // RL
	"heading": "B#7: <cite>Come to the Bank</cite>",
	"blurb": "<p>A man walks through a bank wall using mental power&mdash;but gets trapped halfway, to his colleague's mounting terror.</p>",
	"notes": "institutionalisation, mental breakdown",
	"file": "Bank"
	},
	{
	"code": "B23",
	"heading": "B#23: <cite>Paris Macabre</cite>",
	"blurb": "<p>Two tourists in Paris pay to attend an increasingly strange masque ball.</p>",
	"notes": "beheading, traffic death",
	"file": "Paris"
	},
	{
	"code": "B25",
	"heading": "B#25: <cite>The Flame</cite>",
	"blurb": "<p>A man with an affinity for flames summons a pyromaniac demon to his fireplace.</p>",
	"notes": "arson, fire deaths, obsession",
	"file": "Flame"
	},
	{
	"code": "C06",
	"heading": "C#6: <cite>The Ghost on the Newsreel Negative</cite>",
	"blurb": "<p>Two reporters interview a ghost.</p>",
	"notes": "darkness",
	"file": "Newsreel"
	}
]
},
{
"code": "Mcb",
"heading": "<cite>Macabre</cite>",
"blurb": "<p>A short 1960s radio horror series by the <abbr class=\"unmodified-abbr\" title=\"United States\">US</abbr> Armed Forces Radio Service in Tokyo, written by and starring William Verdier.</p>",
"source": "<a href=\"https://archive.org/details/Macabre_35\" rel=\"external\">Internet Archive</a>",
"shows": [
	{
	"code": "3", // RL
	"heading": "#3: <cite>The Man in the Mirror</cite>",
	"blurb": "<p>A speeding driver makes a deal with the Devil to live one more week after a car crash rips his face off.</p>",
	"notes": "",
	"file": "Mirror"
	},
	{
	"code": "5",
	"heading": "#5: <cite>The Midnight Horseman</cite>",
	"blurb": "<p>A medieval painting's occult curse ensnares its new owner in hypnotic horror.</p>",
	"notes": "abduction",
	"file": "Horseman"
	},
	{
	"code": "7", // RL
	"heading": "#7: <cite>The Crystalline Man</cite>",
	"blurb": "<p>An ancient man of living crystal is exhumed from Arctic ice and resurrects by feeding on human life.</p>",
	"notes": "",
	"file": "Crystalline"
	} 
]
},
{
"code": "McT",
"heading": "<cite>The Mercury Theatre</cite>",
"blurb": "<p>A 1938 extension of Orson Welles' Mercury Theatre to adapt classic fiction to the airwaves, with each show starring Welles himself in a major role.</p>",
"source": "<a href=\"https://archive.org/details/OrsonWelles_MercuryTheatre\" rel=\"external\">Internet Archive</a>",
"shows": [
	{
	"code": "01a",
	"heading": "#1: <cite>Dracula</cite>, part 1",
	"blurb": "<p>A solicitor, his wife, and her suitors band together with a vampire hunter to slay Count Dracula. Adapted from a story by Bram Stoker.</p>",
	"notes": "confinement",
	"file": "Dracula-pt1"
	},
	{
	"code": "01b",
	"heading": "#1: <cite>Dracula</cite>, part 2",
	"blurb": "<p>A solicitor, his wife, and her suitors band together with a vampire hunter to slay Count Dracula. Adapted from a story by Bram Stoker.</p>",
	"notes": "mind control",
	"file": "Dracula-pt2"
	},
	{
	"code": "17a",
	"heading": "#17: <cite>The War of the Worlds</cite>, part 1",
	"blurb": "<p>An adaptation of H.&thinsp;G.&thinsp;Wells' story of Martians invading Earth; some listeners infamously believed it was a description of real current events.</p>",
	"notes": "asphyxiation, cannon-fire (30:39, 30:55, 31:15, 31:49) poison-gassing",
	"file": "Worlds-pt1"
	},
	{
	"code": "17b",
	"heading": "#17: <cite>The War of the Worlds</cite>, part 2",
	"blurb": "<p>An adaptation of H.&thinsp;G.&thinsp;Wells' story of Martians invading Earth; some listeners infamously believed it was a description of real current events.</p>",
	"notes": "asphyxiation, plane crash, poison-gassing",
	"file": "Worlds-pt2"
	}
]
},
{
"code": "Mw",
"heading": "<cite>Mindwebs</cite>",
"blurb": "<p>A 1975&ndash;84 series of sci-fi, fantasy, and horror short story readings by Michael Hanson, who also chose the often-jazzy musical accompaniment for each episode.</p>",
"source": "<a href=\"https://archive.org/details/MindWebs_201410\" rel=\"external\">Internet Archive</a>",
"shows": [
	{
	"code": "001",
	"heading": "#1: <cite>Carcinoma Angels</cite>",
	"blurb": "<p>A genius billionaire with unnatural luck desperately uses a cocktail of hallucinogens to cure his cancer. Written by Norman Spinrad.</p>",
	"notes": "injection, institutionalisation, racism, sterilisation",
	"file": "Carcinoma"
	},
	{
	"code": "003",
	"heading": "#3: <cite>Descending</cite>",
	"blurb": "<p>A bullshit artist on a department store spending spree winds up in a never-ending escalator ride&hellip; Written by Thomas M.&thinsp;Disch.</p>",
	"notes": "head injury, mental breakdown, starvation",
	"file": "Descending"
	},
	{
	"code": "007",
	"heading": "#7: <cite>Roller Ball Murder</cite>",
	"blurb": "<p>The bloodsport of the future grows more deadly by the year, and its most lauded champions are shells of human beings. Written by William Harrison.</p>",
	"notes": "",
	"file": "Ball"
	},
	{
	"code": "012",
	"heading": "#12: <cite>The Swimmer</cite>",
	"blurb": "<p>A man lounging in a friend's garden pool decides on a whim to swim home through his neighbours' pools. Written by John Cheever.</p>",
	"notes": "alcohol, dementia?, foot injury, humiliation",
	"file": "Swimmer"
	},
	{
	"code": "025",
	"heading": "#25: <cite>The Garden of Time</cite>",
	"blurb": "<p>A nobleman and his wife cut the flowers of time to set back the vast waves of humanity that press down on their villa. Written by J.&thinsp;G.&thinsp;Ballard.</p>",
	"notes": "",
	"file": "Garden"
	},
	{
	"code": "026",
	"heading": "#26: <cite>Test</cite> and <cite>The Nine Billion Names of God</cite>",
	"blurb": "<p>A man passes, then fails, a driving test. Technology helps Tibetan monks write the many names of god. Written by Theodore Thomas and Arthur C.&thinsp;Clarke.</p>",
	"notes": "car crash, institutionalisation",
	"file": "Test-Names"
	},
	{
	"code": "031",
	"heading": "#31: <cite>In the Abyss</cite>",
	"blurb": "<p>Wondrous and deadly secrets await the first human explorer to the ocean floor. Written by H.&thinsp;G.&thinsp;Wells.</p>",
	"notes": "claustrophobia, darkness, drowning",
	"file": "Abyss"
	},
	{
	"code": "032", // RL
	"heading": "#32: <cite>A Night in Elf Hill</cite>",
	"blurb": "<p>An ancient alien pleasure dome is hungry to serve anyone it can since its makers abandoned it aeons ago. Written by Norman Spinrad.</p>",
	"notes": "abandonment, cannibalism?",
	"file": "Elf"
	},
	{
	"code": "033",
	"heading": "#33: <cite>The Top</cite>",
	"blurb": "<p>An advertiser for a colossal corporation discovers the secret at the summit of its pyramidal headquarters. Written by George Sumner Albee.</p>",
	"notes": "overwork death",
	"file": "Top"
	},
	{
	"code": "051",
	"heading": "#51: <cite>The Evergreen Library</cite>",
	"blurb": "<p>A lawyer visiting a dead client's estate loses himself in the old man's library. Written by Bill Pronzini and Jeffrey Wallmann.</p>",
	"notes": "derealisation",
	"file": "Evergreen"
	},
	{
	"code": "057",
	"heading": "#57: <cite>The End</cite>",
	"blurb": "<p>One man keeps building, at the end of all things. Written by Ursula K.&thinsp;Le Guin.</p>",
	"notes": "",
	"file": "End"
	},
	{
	"code": "066",
	"heading": "#66: <cite>Desertion</cite>",
	"blurb": "<p>Human minds are transposed into Jupiter's fauna to explore and colonise the planet&mdash;but they all desert the mission. Written by Clifford Simak.</p>",
	"notes": "",
	"file": "Desertion"
	},
	{
	"code": "071",
	"heading": "#71: <cite>Gas Mask</cite>",
	"blurb": "<p>A massive city descends into total merciless gridlock. Written by James Houston.</p>",
	"notes": "",
	"file": "Mask"
	},
	{
	"code": "075",
	"heading": "#75: <cite>A Walk in the Dark</cite>",
	"blurb": "<p>A lost astronaut takes a six mile walk back to base on a dark dead planet. How dangerous could it be? Written by Arthur C.&thinsp;Clarke.</p>",
	"notes": "",
	"file": "Walk"
	},
	{
	"code": "076",
	"heading": "#76: <cite>When We Went To See The End Of The World</cite>",
	"blurb": "<p>Time-travelling tourists boast about seeing the end of the world&mdash;until they realise they didn't all see the <em>same</em> end&hellip; Written by Robert Silverberg.</p>",
	"notes": "",
	"file": "End"
	},
	{
	"code": "082",
	"heading": "#82: <cite>The Plot is the Thing</cite> and <cite>Midnight Express</cite>",
	"blurb": "<p>Doctors lobotomise a <abbr class=\"unmodified-abbr\" title=\"television\">TV</abbr>-obsessed woman, with bizarre results. A young man meets his childhood terror. Written by Robert Block and Alfred Noyes.</p>",
	"notes": "derealisation, injection, institutionalisation, lobotomy, nightmares, racism",
	"file": "Plot-Express"
	},
	{
	"code": "088",
	"heading": "#88: <cite>Nackles</cite>",
	"blurb": "<p>An abusive father invents an evil opposite to Santa Claus to terrify his kids, with severe consequences. Written by Curt Clark.</p>",
	"notes": "",
	"file": "Nackles"
	},
	{
	"code": "094",
	"heading": "#94: <cite>But As a Soldier, for His Country</cite>",
	"blurb": "<p>The state refuses to let its best soldier die, no matter how many wars he wins. Written by Stephen Goldin.</p>",
	"notes": "",
	"file": "Soldier"
	},
	{
	"code": "095",
	"heading": "#95: <cite>Winter Housekeeping</cite> and <cite>The Public Hating</cite>",
	"blurb": "<p>A woman takes heavy measures against the poison of ageing. Mass telekinesis is used in horrific new executions. Written by Molly Daniel and Steve Allen.</p>",
	"notes": "asphyxiation, obsession",
	"file": "Winter-Hating"
	},
	{
	"code": "098",
	"heading": "#98: <cite>To See the Invisible Man</cite>",
	"blurb": "<p>A man is officially punished for his &ldquo;coldness&rdquo; by being utterly ignored by everyone in society. Written by Robert Silverberg.</p>",
	"notes": "car attack, medical neglect, suicide ideation, voyeurism",
	"file": "Invisible"
	},
	{
	"code": "099",
	"heading": "#99: <cite>Ghosts</cite> and <cite>The Diggers</cite>",
	"blurb": "<p>Robots struggle with reproduction. A woman seeks the mysterious Diggers for love of mystery itself. Written by Robert F.&thinsp;Young and Don Stern.</p>",
	"notes": "suicide",
	"file": "Ghosts-Diggers"
	},
	{
	"code": "105",
	"heading": "#105: <cite>Silenzia</cite>",
	"blurb": "<p>A man discovers an air-spray that eats sound; he slowly gives in to the temptation of silence. Written by Alan Nelson.</p>",
	"notes": "",
	"file": "Silenzia"
	},
	{
	"code": "106",
	"heading": "#106: <cite>Deaf Listener</cite> and <cite>Shall the Dust Praise Thee?</cite>",
	"blurb": "<p>A telepath employed to detect alien life makes a critical error. The Day of Reckoning suffers a hitch. Written by Rachel Payes and Damon Knight.</p>",
	"notes": "",
	"file": "Listener-Dust"
	},
	{
	"code": "110",
	"heading": "#110: <cite>Appointment at Noon</cite> and <cite>Man in a Quandary</cite>",
	"blurb": "<p>A suspicious man has a suspicious visitor. Someone writes to an advice column for help with a unique problem. Written by Eric Russell and L.&thinsp;J.&thinsp;Stecher.</p>",
	"notes": "ableism",
	"file": "Noon-Quandary"
	},
	{
	"code": "115",
	"heading": "#115: <cite>The Fog Horn</cite>",
	"blurb": "<p>An old fog horn's deafening cry calls forth an ancient titan. Written by Ray Bradbury.</p>",
	"notes": "",
	"file": "Horn"
	},
	{
	"code": "117",
	"heading": "#117: <cite>The Petrified World</cite>",
	"blurb": "<p>A nervous man shifts between surreal dream and real nightmare. Written by Robert Sheckley.</p>",
	"notes": "near-drowning, nightmares",
	"file": "Petrified"
	},
	{
	"code": "118",
	"heading": "#118: <cite>Island of Fear</cite>",
	"blurb": "<p>An art collector obsesses over the perfect statuary beyond the monolithic wall on a tiny Greek island. Written by William Sambrot.</p>",
	"notes": "near-drowning",
	"file": "Island"
	},
	{
	"code": "121",
	"heading": "#121: <cite>The Word</cite> and <cite>Stair Trick</cite>",
	"blurb": "<p>Stranded aliens go out on a strange night in search of food. A bartender uses a practical joke to escape the dullness of life. Written by Mildred Clingerman.</p>",
	"notes": "",
	"file": "Word-Trick"
	},
	{
	"code": "124",
	"heading": "#124: <cite>After the Myths Went Home</cite>",
	"blurb": "<p>Humanity calls forth old heroes and gods, only to decide they're more trouble than they're worth. Written by Robert Silverberg.</p>",
	"notes": "",
	"file": "Myths"
	},
	{
	"code": "125",
	"heading": "#125: <cite>The Rules of the Road</cite>",
	"blurb": "<p>An alien ship lands in the desert. None who enter survive&mdash;until one man finds within it the awful secrets of the universe. Written by Norman Spinrad.</p>",
	"notes": "imprisonment, psychological torture",
	"file": "Rules"
	},
	{
	"code": "131",
	"heading": "#131: <cite>The Racer</cite>",
	"blurb": "<p>One of the greatest drivers of a near-future death race comes to grips with how people really feel about his murderous deeds. Written by Ib Melchior.</p>",
	"notes": "animal deaths, car crash",
	"file": "Racer"
	},
	{
	"code": "133",
	"heading": "#133: <cite>None Before Me</cite>",
	"blurb": "<p>A collector of the very best of everything obsesses over an exquisitely life-like dollhouse. Written by Sidney Carroll.</p>",
	"notes": "fatphobia",
	"file": "None"
	},
	{
	"code": "139",
	"heading": "#139: <cite>The Eternal Machines</cite>",
	"blurb": "<p>The lonely warden of a scrap planet builds a museum of machines he intends to outlast humanity. Written by William Spencer.</p>",
	"notes": "",
	"file": "Eternal"
	},
	{
	"code": "141",
	"heading": "#141: <cite>The Sound Machine</cite>",
	"blurb": "<p>An inventor makes a machine that detect sounds beyond human hearing and discovers a world of pain in the back garden. Written by Roald Dahl.</p>",
	"notes": "",
	"file": "Sound"
	},
	{
	"code": "147",
	"heading": "#147: <cite>The Maze</cite>",
	"blurb": "<p>A stranded scientist experimenting on mice finds herself slipping into the centre of the maze. Written by Stuart Dybek.</p>",
	"notes": "animal cannibalism, bestiality, imprisonment, rape, rotting animals, torture",
	"file": "Maze"
	},
	{
	"code": "149",
	"heading": "#149: <cite>The Worm</cite>",
	"blurb": "<p>An old man refuses to leave his home even as it's crushed by a giant worm. Written by David Keller.</p>",
	"notes": "",
	"file": "Worm"
	},
	{
	"code": "155",
	"heading": "#155: <cite>The Valley of Echoes</cite>",
	"blurb": "<p>Lonely astronauts trawling for evidence of Martian life discover the voices of people long-gone or yet to come. Written by Gerard Klein.</p>",
	"notes": "",
	"file": "Echoes"
	},
	{
	"code": "160",
	"heading": "#160: <cite>Letter to a Phoenix</cite>",
	"blurb": "<p>An immortal writes a letter of advice from over a hundred millennia of solitude and thought. Written by Fredric Brown.</p>",
	"notes": "",
	"file": "Phoenix"
	},
	{
	"code": "162",
	"heading": "#162: <cite>The Vertical Ladder</cite>",
	"blurb": "<p>A young boy, dared by his peers, climbs the ladder of a towering gasometer. Written by William Sansom.</p>",
	"notes": "abandonment, ableism, child endangered",
	"file": "Ladder"
	},
	{
	"code": "165",
	"heading": "#165: <cite>Restricted Area</cite>",
	"blurb": "<p>A spaceship crew scientifically investigate an eerily perfect planet. Written by Robert Sheckley.</p>",
	"notes": "",
	"file": "Restricted"
	},
	{
	"code": "169",
	"heading": "#169: <cite>The Available Data on the Worp Reaction</cite>",
	"blurb": "<p>An intellectually disabled boy gathers scrap from the city junkyard and builds a machine that confounds all observers. Written by Lion Miller.</p>",
	"notes": "",
	"file": "Worp"
	},
	{
	"code": "176",
	"heading": "#176: <cite>Transformer</cite>",
	"blurb": "<p>The quaint railway town of Elm Point is switched off, boxed away, and sold on. Written by Chad Oliver.</p>",
	"notes": "",
	"file": "Transformer"
	},
	{
	"code": "187", // RL
	"heading": "#187: <cite>Null-P</cite>",
	"blurb": "<p>An extraordinarily ordinary man becomes vital to society after apocalypse. Written by William Tenn.</p>",
	"notes": "",
	"file": "Null"
	},
	{
	"code": "237",
	"heading": "#237: <cite>The Star</cite> and <cite>The Gift</cite>",
	"blurb": "<p>An apocalyptic blaze&mdash;a guiding star. A boy receives a wondrous Christmas gift. Written by Arthur C.&thinsp;Clarke and Ray Bradbury.</p>",
	"notes": "",
	"file": "Star-Gift"
	}
]
},
{
"code": "MsT",
"heading": "<cite>The Mysterious Traveler</cite>",
"blurb": "<p>A 1943&ndash;52 anthology of horror stories hosted and narrated by the titular Mysterious Traveler riding a train racing through the night.</p>",
"source": "<a href=\"https://archive.org/details/OTRR_Mysterious_Traveler_Singles\" rel=\"external\">Internet Archive</a>",
"shows": [
	{
	"code": "111",
	"heading": "#111: <cite>The Locomotive Ghost</cite>",
	"blurb": "<p>Two men derail and rob a payday train, only to suffer the wrath of the Judgement Special&mdash;a ghostly train that hunts those who defile the railways.</p>",
	"notes": "asphyxiation, gunshots (6:44, 7:28), rail crash, paranoia",
	"file": "Locomotive"
	},
	{
	"code": "256",
	"heading": "#256: <cite>The Lady in Red</cite>",
	"blurb": "<p>A reporter follows a trail of mob murders attended by a mysterious woman in a striking red dress.</p>",
	"notes": "alcohol, fall death, gunshots (4:38&ndash;40, 4:50&ndash;53, 24:06)",
	"file": "Red"
	},
	{
	"code": "344",
	"heading": "#344: <cite>Strange New World</cite>",
	"blurb": "<p>Two pilots make an emergency landing on the ocean and drift to Bikini Atoll, where they discover how nuclear tests have warped the local wildlife.</p>",
	"notes": "",
	"file": "Strange"
	}
]
},
{
"code": "Nf",
"heading": "<cite>Nightfall</cite>",
"blurb": "<p>A 1980&ndash;83 Canadian series of original and adapted horror stories created by Bill Howell.</p>",
"source": "<a href=\"https://archive.org/details/CBC_NightfallOTR\" rel=\"external\">Internet Archive</a>",
"shows": [
	{
	"code": "003",
	"heading": "#3: <cite>Welcome to Homerville</cite>",
	"blurb": "<p>A trucker follows a sinister radio siren's call.</p>",
	"notes": "truck crash",
	"file": "Homerville"
	},
	{
	"code": "014",
	"heading": "#14: <cite>The Stone Ship</cite>",
	"blurb": "<p>A ship finds a massive vessel of stone out on the open sea, full of treasures and monsters. Adapted from a story by William Hope Hodgson.</p>",
	"notes": "drowning, gunshots (19:24&ndash;29)",
	"file": "Ship"
	},
	{
	"code": "017",
	"heading": "#17: <cite>Last Visit</cite>",
	"blurb": "<p>A couple take a backroad and wind up in a desolate, foggy town inhabited by one old, old man.</p>",
	"notes": "gore, mental breakdown",
	"file": "Visit"
	},
	{
	"code": "018",
	"heading": "#18: <cite>Ringing the Changes</cite>",
	"blurb": "<p>Honeymooners in tiny coastal town find the ocean missing, the people unwelcoming, and the bells all ringing louder every hour.</p>",
	"notes": "break-in, forced stripping, mob attack, sexual assault (themes?)",
	"file": "Ringing"
	},
	{
	"code": "034",
	"heading": "#34: <cite>The Book of Hell</cite>",
	"blurb": "<p>Three publishers read the &ldquo;Book of Hell&rdquo;, an account of infernal torment written by a man who's been dead for years.</p>",
	"notes": "explosion (24:56), injections, involuntary medical experiments",
	"file": "Hell"
	},
	{
	"code": "042",
	"heading": "#42: <cite>In the Name of the Father</cite>",
	"blurb": "<p>A grieving writer recovers in an old fishing town with a deep link to the sharks that swim its waters.</p>",
	"notes": "implied bestiality, parental death, pregnancy",
	"file": "Father"
	},
	{
	"code": "062",
	"heading": "#62: <cite>The Road Ends at the Sea</cite><!--title listed in the Internet Archive collection appears to be incorrect-->",
	"blurb": "<p>A couple of lighthouse keepers' solitude is broken by the arrival of an old &ldquo;friend&rdquo;&mdash;and romantic rival&mdash;and a massive black freighter just offshore.</p>",
	"notes": "",
	"file": "Sea"
	},
	{
	"code": "075",
	"heading": "#75: <cite>Lazarus Rising</cite>",
	"blurb": "<p>A reporter investigates a small-town resurrection and slowly uncovers the festering truth.</p>",
	"notes": "arson, phone eavesdropping",
	"file": "Lazarus"
	},
	{
	"code": "100",
	"heading": "#100: <cite>Waters Under the Bridge</cite>",
	"blurb": "<p>Divers hunt for treasure in the sunken ruins of a road bridge, until bad luck gets them one by one.</p>",
	"notes": "adult child death, drowning, mention of institutionalisation, suicide, survivor's guilt",
	"file": "Bridge"
	}
]
},
{
"code": "PC",
"heading": "the Pegāna Cycle",
"blurb": "<p>Lord Dunsany's mythology cycle of weird and terrible gods and their deeds and misdeeds: <cite>The Gods of Pegāna</cite> (1905), <cite>Time and the Gods</cite> (1906), and the dream-stories collected in <cite>Tales of Three Hemispheres</cite> (1919).</p>",
"source": "<a href=\"https://librivox.org\" rel=\"external\">LibriVox</a>",
"shows": [
	{
	"code": "01",
	"heading": "book 1: <cite>The Gods of Pegāna</cite>, part 1",
	"blurb": "<p>From the preface to <cite>Revolt of the Home Gods</cite>. Read by Jason Mills.</p>",
	"notes": "",
	"file": "GoP-1"
	},
	{
	"code": "02",
	"heading": "book 1: <cite>The Gods of Pegāna</cite>, part 2",
	"blurb": "<p>From <cite>Of Dorozhand</cite> to <cite>Of How the Gods Whelmed Sidith</cite>. Read by Jason Mills.</p>",
	"notes": "",
	"file": "GoP-2"
	},
	{
	"code": "03",
	"heading": "book 1: <cite>The Gods of Pegāna</cite>, part 3",
	"blurb": "<p>From <cite>Of How Imbaun Became High Prophet in Aradec of All the Gods Save One</cite> to <cite>The Bird of Doom and the End</cite>. Read by Jason Mills.</p>",
	"notes": "",
	"file": "GoP-3"
	},
	{
	"code": "04",
	"heading": "book 2: <cite>Time and the Gods</cite>, part 1",
	"blurb": "<p>From the preface to <cite>A Legend of the Dawn</cite>. Read by KentF.</p>",
	"notes": "",
	"file": "TG-1"
	},
	{
	"code": "05",
	"heading": "book 2: <cite>Time and the Gods</cite>, part 2",
	"blurb": "<p>From <cite>The Vengeance of Men</cite> to <cite>The Caves of Kai</cite>. Read by KentF, RedToby, and hefyd.</p>",
	"notes": "",
	"file": "TG-2"
	},
	{
	"code": "06",
	"heading": "book 2: <cite>Time and the Gods</cite>, part 3",
	"blurb": "<p>From <cite>The Sorrow of Search</cite> to <cite>For the Honour of the Gods</cite>. Read by Le Scal and hefyd.</p>",
	"notes": "",
	"file": "TG-3"
	},
	{
	"code": "07",
	"heading": "book 2: <cite>Time and the Gods</cite>, part 4",
	"blurb": "<p>From <cite>Night and Morning</cite> to <cite>The South Wind</cite>. Read by Måns Broo.</p>",
	"notes": "",
	"file": "TG-4"
	},
	{
	"code": "08",
	"heading": "book 2: <cite>Time and the Gods</cite>, part 5",
	"blurb": "<p>From <cite>In the Land of Time</cite> to <cite>The Dreams of the Prophet</cite>. Read by RedToby and hefyd.</p>",
	"notes": "",
	"file": "TG-5"
	},
	{
	"code": "09",
	"heading": "book 2: <cite>Time and the Gods</cite>, part 6",
	"blurb": "<p><cite>The Journey of the King</cite>, parts &#x2160;&ndash;&#x2166;. Read by Kevin McAsh, Måns Broo, and Robin Cotter.</p>",
	"notes": "alcohol",
	"file": "TG-6"
	},
	{
	"code": "10",
	"heading": "book 2: <cite>Time and the Gods</cite>, part 7",
	"blurb": "<p><cite>The Journey of the King</cite>, parts &#x2167;&ndash;&#x216a;. Readings by Robin Cotter and Elmensdorp.</p>",
	"notes": "alcohol",
	"file": "TG-7"
	},
	{
	"code": "11",
	"heading": "dreams: <cite>Idle Days on the Yann</cite>",
	"blurb": "<p>A dreamer travels the river Yann aboard the ship <i>Bird of the River</i>, stopping in bizarre cities of wonders and monsters along the way. Read by Alex Clarke.</p>",
	"notes": "alcohol",
	"file": "Yann"
	},
	{
	"code": "12",
	"heading": "dreams: <cite>A Shop in Go-By Street</cite>",
	"blurb": "<p>The dreamer returns to the Yann via a strange shop of myths and rareties, seeking his friends on the <i>Bird of the River</i>. Read by Ed Humpal.</p>",
	"notes": "alcohol",
	"file": "Shop"
	},
	{
	"code": "13",
	"heading": "dreams: <cite>The Avenger of Perdóndaris</cite>",
	"blurb": "<p>The dreamer returns once more and visits the palace of Singanee, avenger of ruined Perdóndaris, and grows weary of dreams. Read by Ed Humpal.</p>",
	"notes": "",
	"file": "Avenger"
	}
]
},
{
"code": "QP",
"heading": "<cite>Quiet, Please</cite>",
"blurb": "<p>A 1947&ndash;49 radio horror anthology written by Wyllis Cooper. It starred radio announcer Ernest Chappell (his only acting role), who often spoke informally and directly to the audience.</p>",
"source": "<a href=\"https://www.quietplease.org\" rel=\"external\">quietplease.org</a>",
"shows": [
	{
	"code": "001",
	"heading": "#1: <cite>Nothing Behind the Door</cite>",
	"blurb": "<p>Bank robbers try to hide the money in a mountain shed that contains nothing. Literally <em>nothing</em>.</p>",
	"notes": "",
	"file": "Nothing"
	},
	{
	"code": "004",
	"heading": "#4: <cite>The Ticket Taker</cite>",
	"blurb": "<p>A group of killers trying to escape Chicago with their money attract the attention of the Ticket Taker.</p>",
	"notes": "",
	"file": "Ticket"
	},
	{
	"code": "006",
	"heading": "#6: <cite>I Remember Tomorrow</cite>",
	"blurb": "<p>A time-traveller explains why three criminals <em>must</em> be stopped&mdash;and why <em>you</em> will stop them.</p>",
	"notes": "alcohol, betrayal, gunshots (26:16)",
	"file": "Tomorrow"
	},
	{
	"code": "007",
	"heading": "#7: <cite>Inquest</cite>",
	"blurb": "<p>A heartless man faces a murder inquest like no other&mdash;a jury in costume, an audience of millions, and the mysterious Coroner&hellip;</p>",
	"notes": "ableism, broken arm (poorly healed), domestic abuse, financial abuse",
	"file": "Inquest"
	},
	{
	"code": "009",
	"heading": "#9: <cite>A Mile High and a Mile Deep</cite> (script read)",
	"blurb": "<p>The Earth takes its due in the mines far below Butte, Montana&mdash;the city <q>a mile high and a mile deep</q>. An amateur reading of this lost episode's script.</p>",
	"notes": "claustrophobia, darkness",
	"file": "Mile"
	},
	{
	"code": "019",
	"heading": "#19: <cite>Camera Obscura</cite>",
	"blurb": "<p>A remorseless killer's victim haunts him through the miniature world of the camera obscura.</p>",
	"notes": "abandonment, drowning",
	"file": "Obscura"
	},
	{
	"code": "021",
	"heading": "#21: <cite>Don't Tell <em>Me</em> About Halloween</cite>",
	"blurb": "<p>A man married to a witch lives forever&mdash;at the cost of suffering her jealousy and sadism.</p>",
	"notes": "",
	"file": "Halloween"
	},
	{
	"code": "022",
	"heading": "#22: <cite>Take Me Out to the Graveyard</cite>",
	"blurb": "<p>A taxi driver tells how all his passengers want to go to the graveyard&mdash;and how they die along the way.</p>",
	"notes": "",
	"file": "Graveyard"
	},
	{
	"code": "023",
	"heading": "#23: <cite>Three</cite>",
	"blurb": "<p>A man tells how the number three seems to have a sinister, even murderous hold over him.</p>",
	"notes": "hallucination?, obsession",
	"file": "Three"
	},
	{
	"code": "024",
	"heading": "#24: <cite>Kill Me Again</cite>",
	"blurb": "<p>A decent man finds himself being murdered over and over again after making a deal with the devil.</p>",
	"notes": "gunshots (2:16)",
	"file": "Kill"
	},
	{
	"code": "027",
	"heading": "#27: <cite>Some People Don't Die</cite>",
	"blurb": "<p>Archaeologists seek the ruins of an ancient society in a desert mesa, only to find the people aren't entirely dead.</p>",
	"notes": "desiccation, imprisonment, racism, snake bite",
	"file": "Die"
	},
	{
	"code": "030",
	"heading": "#30: <cite>Rain on New Year's Eve</cite>",
	"blurb": "<p>An overworked horror screenwriter makes monsters for his micro-managing, incompetent director.</p>",
	"notes": "",
	"file": "Rain"
	},
	{
	"code": "034",
	"heading": "#34: <cite>Green Light</cite>",
	"blurb": "<p>A railwayman tells the tale of how he lost his leg in an impossible train crash.</p>",
	"notes": "",
	"file": "Light"
	},
	{
	"code": "037",
	"heading": "#37: <cite>Whence Came You?</cite>",
	"blurb": "<p>An archaeologist meets the past in Cairo and entombed below the desert.</p>",
	"notes": "bite injury, claustrophobia, darkness",
	"file": "Whence"
	},
	{
	"code": "038",
	"heading": "#38: <cite>Wear the Dead Man's Coat</cite>",
	"blurb": "<p>A homeless beggar kills a man for his coat, then remembers a saying: <q>Wear the dead man's coat, none will take note</q>.</p>",
	"notes": "alcohol, betrayal",
	"file": "Coat"
	},
	{
	"code": "040",
	"heading": "#40: <cite>Never Send to Know</cite>",
	"blurb": "<p>A ghost hires a skeptical <abbr class=\"unmodified-abbr\">PI</abbr> to solve his own murder.</p>",
	"notes": "blood, gunshot (2:20), implied suicide",
	"file": "Send"
	},
	{
	"code": "042",
	"heading": "#42: <cite>A Night to Forget</cite>",
	"blurb": "<p>A radio actor has nightmares of his impending death that slowly seep into reality.</p>",
	"notes": "funeral arrangements, gunshots (26:10&ndash;26:16), murder",
	"file": "Forget"
	},
	{
	"code": "045",
	"heading": "#45: <cite>12 to 5</cite>",
	"blurb": "<p>A graveyard shift radio <abbr class=\"unmodified-abbr\" title=\"disc jockey\">DJ</abbr> has an unusual visitor&mdash;a news-reader who reads the future.</p>",
	"notes": "",
	"file": "Twelve"
	},
	{
	"code": "046",
	"heading": "#46: <cite>Clarissa</cite>",
	"blurb": "<p>A man seeks lodging at a decrepit house inhabited by an ancient father and his unseen daughter.</p>",
	"notes": "",
	"file": "Clarissa"
	},
	{
	"code": "047",
	"heading": "#47: <cite>Thirteen and Eight</cite>",
	"blurb": "<p>An opportunistic news photographer is plagued by a photobomber only he can see.</p>",
	"notes": "fall injury, gunshots (7:47), traffic death",
	"file": "Thirteen"
	},
	{
	"code": "048",
	"heading": "#48: <cite>How Beautiful Upon the Mountain</cite>",
	"blurb": "<p>Two men seek to summit Everest&mdash;giant mountain&mdash;home of the gods&mdash;beautiful, deadly bride.</p>",
	"notes": "",
	"file": "Mountain"
	},
	{
	"code": "049", // RL
	"heading": "#49: <cite>There Are Shadows Here</cite>",
	"blurb": "<p>A shadow of a woman seeks her beloved at his favourite bar.</p>",
	"notes": "",
	"file": "Shadows"
	}, 
	{
	"code": "050",
	"heading": "#50: <cite>Gem of Purest Ray</cite>",
	"blurb": "<p>A murderer explains his motive: his victims were agents of an apocalyptic Atlantean conspiracy.</p>",
	"notes": "betrayal, conspiracism, paranoia",
	"file": "Ray"
	},
	{
	"code": "055", // RL
	"heading": "#55: <cite>Let the Lilies Consider</cite>",
	"blurb": "<p>A suspected murderer recounts the triangle of love and hate between he, his wife, and his beloved lilies.</p>",
	"notes": "gunshot (19:01), neglect, suicide",
	"file": "Lilies"
	},
	{
	"code": "058",
	"heading": "#58: <cite>The Man Who Stole a Planet</cite>",
	"blurb": "<p>Archaeologists find the planet Earth in an ancient Mayan temple.</p>",
	"notes": "domestic abuse, natural disaster",
	"file": "Planet"
	},
	{
	"code": "060",
	"heading": "#60: <cite>The Thing on the Fourble Board</cite>",
	"blurb": "<p>An oil derrick pulls an invisible creature from the depths of the earth.</p>",
	"notes": "betrayal, spiders?",
	"file": "Fourble"
	},
	{
	"code": "065",
	"heading": "#65: <cite>Symphony in D Minor</cite>",
	"blurb": "<p>A vengeful hypnotist uses Cesar Franck's <cite>Symphony in D Minor</cite>, the series' theme music, to wreak revenge on his adulterous wife and her lover.</p>",
	"notes": "fall death, stab death",
	"file": "Symphony"
	},
	{
	"code": "067",
	"heading": "#67: <cite>Light the Lamp for Me</cite>",
	"blurb": "<p>An old man's oil lamp lets him travel to the past as often as he likes&mdash;but only once to the future.</p>",
	"notes": "disease, loneliness",
	"file": "Lamp"
	},
	{
	"code": "068",
	"heading": "#68: <cite>Meet John Smith, John</cite>",
	"blurb": "<p>A man gives to a beggar who just so happens to share his name&mdash;a lot more than his name, in fact.</p>",
	"notes": "adultery, alcohol, murder",
	"file": "Meet"
	},
	{
	"code": "069",
	"heading": "#69: <cite>Beezer's Cellar</cite>",
	"blurb": "<p>Bank robbers hide the loot in an allegedly haunted cellar, betting the legend will keep prying eyes away.</p>",
	"notes": "",
	"file": "Cellar"
	},
	{
	"code": "071",
	"heading": "#71: <cite>Good Ghost</cite>",
	"blurb": "<p>A murder victim won't stop trying to help his killer.</p>",
	"notes": "abandonment, betrayal, domestic abuse, gambling, institutionalisation",
	"file": "Ghost"
	},
	{
	"code": "072",
	"heading": "#72: <cite>Calling All Souls</cite>",
	"blurb": "<p>Halloween: the dead rise to give a death row inmate one last chance to prove his innocence.</p>",
	"notes": "betrayal",
	"file": "Souls"
	},
	{
	"code": "073",
	"heading": "#73: <cite>Adam and the Darkest Day</cite>",
	"blurb": "<p>Planet Earth says &ldquo;Goodbye Goldilocks&rdquo;.</p>",
	"notes": "claustrophobia, darkness, nuclear war",
	"file": "Adam"
	},
	{
	"code": "076",
	"heading": "#76: <cite>My Son, John</cite>",
	"blurb": "<p>A bereaved father begs a wise woman for any way to bring his son back&mdash;no matter the danger, the cost.</p>",
	"notes": "animal attacks, bite injury",
	"file": "John"
	},
	{
	"code": "081", // RL
	"heading": "#81: <cite>The Time of the Big Snow</cite>",
	"blurb": "<p>Two young children lose their way in a blizzard and discover the truth behind the old saying about snow: <q>The old woman's picking her geese</q>.</p>",
	"notes": "",
	"file": "Snow"
	},
	{
	"code": "083",
	"heading": "#83: <cite>Is This Murder?</cite>",
	"blurb": "<p>An expert on prosthetic limbs struggles to reason with his assistant, who's obsessed with making an artificial man.</p>",
	"notes": "alcohol, betrayal, murder?",
	"file": "Murder"
	},
	{
	"code": "084",
	"heading": "#84: <cite>Summer Goodbye</cite>",
	"blurb": "<p>Robbers escape from the police&mdash;but can't seem to escape the hitch-hiker they see over and over again.</p>",
	"notes": "brushfire, gunshots (22:20, 23:48, 24:17), unhealthy romantic relationship",
	"file": "Summer"
	},
	{
	"code": "085",
	"heading": "#85: <cite>Northern Lights</cite>",
	"blurb": "<p>Inventors of a time machine discover the monstrous song of the aurora borealis.</p>",
	"notes": "caterpillars, mind control",
	"file": "Northern"
	},
	{
	"code": "088",
	"heading": "#88: <cite>Where Do You Get Your Ideas?</cite>",
	"blurb": "<p>Wyllis Cooper, the series' writer, meets a barfly who wonders where he gets his ideas&mdash;and <em>insists</em> Cooper listens to his own strange story.</p>",
	"notes": "gunshot (12:25)",
	"file": "Ideas"
	},
	{
	"code": "089", // RL
	"heading": "#89: <cite>If I Should Wake Before I Die</cite>",
	"blurb": "<p>One of the world's foremost scientists is too deep in his research to see the apocalyptic consequences.</p>",
	"notes": "natural disasters, nightmare, nuclear war",
	"file": "Wake"
	},
	{
	"code": "090",
	"heading": "#90: <cite>The Man Who Knew Everything</cite>",
	"blurb": "<p>The Man Who Knows Everything explains how you're going to die&mdash;and tries to help you avoid your fate.</p>",
	"notes": "",
	"file": "Everything"
	},
	{
	"code": "092",
	"heading": "#92: <cite>The Smell of High Wines</cite>",
	"blurb": "<p>A distillery worker recalls the dark moments of his life where the smell of high wines presaged death.</p>",
	"notes": "blood, stab death, strangulation, suicide?",
	"file": "Wines"
	},
	{
	"code": "094",
	"heading": "#94: <cite>The Venetian Blind Man</cite>",
	"blurb": "<p>The Man Who Knows Everything returns to identify an impostrous assassin. <strong>Note:</strong> This recording is missing about 5 minutes in the middle.</p>",
	"notes": "",
	"file": "Blind"
	},
	{
	"code": "099",
	"heading": "#99: <cite>The Other Side of the Stars</cite>",
	"blurb": "<p>A treasure hunter and his companion investigate an ancient well and find music from beyond the stars.</p>",
	"notes": "animal deaths, gunshots (13:07), possession?",
	"file": "Stars"
	},
	{
	"code": "100",
	"heading": "#100: <cite>The Little Morning</cite>",
	"blurb": "<p>A hitch-hiker returns to his old home taht burnt down with his beloved inside to sing the song they promised to duet on every birthday.</p>",
	"notes": "drowning, suicide",
	"file": "Morning"
	},
	{
	"code": "103",
	"heading": "#103: <cite>Tanglefoot</cite>",
	"blurb": "<p>A man breeds giant flies whose hunger grows in proportion&hellip; and out of control.</p>",
	"notes": "animal death, betrayal",
	"file": "Tanglefoot"
	},
	{
	"code": "106",
	"heading": "#106: <cite>Quiet, Please</cite>",
	"blurb": "<p>The last man alive tells a story of love, hate, bigotry, supremacism, and war&mdash;all-consuming war.</p>",
	"notes": "",
	"file": "Quiet"
	}
]
},
{
"code": "RCP",
"heading": "<cite>Radio City Playhouse</cite>",
"blurb": "<p>A 1948&ndash;50 anthology of original radio dramas and adaptations, including a few with a touch of the supernatural.</p>",
"source": "<a href=\"https://archive.org/details/radio_city_playhouse_202008\" rel=\"external\">Internet Archive</a>",
"shows": [
	{
	"code": "58",
	"heading": "#58: <cite>The Wind</cite>",
	"blurb": "<p>A former pilot is deathly afraid of the wind, which he believes is out to kill him, but his friends can't&mdash;or won't&mdash;help. Adapted from a story by Ray Bradbury.</p>",
	"notes": "sanism",
	"file": "Wind"
	}
]
},
{
"code": "SA",
"heading": "<cite><abbr>SCP</abbr> Archives</cite>",
"blurb": "<p><q>Secure. Contain. Protect.</q> Readings of the records and tales of the <a href=\"https://scp-wiki.wikidot.com\" rel=\"external\"><abbr>SCP</abbr> Foundation</a>, produced by <a href=\"https://midnightdisease.net\" rel=\"external\">Midnight Disease</a>.</p><!--show titles are taken from the podcast, not the SCP Foundation wiki (some titles differ between the two sources)-->",
"source": "<a href=\"https://www.podbean.com/podcast-detail/vvb44-c07fc/SCP-Archives-Podcast\" rel=\"external\">Podbean</a>, used under the <a href=\"https://creativecommons.org/licenses/by-sa/3.0\" rel=\"external license\">CC BY-SA 3.0 Unported license</a>",
"shows": [
	{
	"code": "00093a",
	"heading": "<cite>SCP-093: &ldquo;Red Sea Object&rdquo;, part 1</cite>",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-093\" rel=\"external\">Full report</a></p>",
	"notes": "body horror, isolation, theocracy",
	"file": "Red-pt1"
	},
	{
	"code": "00093b",
	"heading": "<cite>SCP-093: &ldquo;Red Sea Object&rdquo;, part 2</cite>",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-093\" rel=\"external\">Full report</a></p>",
	"notes": "body horror, isolation, theocracy",
	"file": "Red-pt2"
	},
	{
	"code": "00701",
	"heading": "<cite>SCP-701: &ldquo;The Hanged King&rdquo;</cite>",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-701\" rel=\"external\">Full report</a></p>",
	"notes": "cannibalism, child death, hanging, mind control, suicide",
	"file": "Hanged"
	},
	{
	"code": "01233-04233",
	"heading": "<cite>SCP-1233 & 4233: &ldquo;The Sky & The Sea&rdquo;</cite>",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-1233\" rel=\"external\">Full report (Sky)</a><br><a href=\"https://scp-wiki.wikidot.com/scp-4233\" rel=\"external\">Full report (Sea)</a></p>",
	"notes": "",
	"file": "Sky-Sea"
	},
	{
	"code": "01861",
	"heading": "<cite>SCP-1861: &ldquo;Crew of the <abbr>HMS</abbr> Wintersheimer&rdquo;</cite>",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-1861\" rel=\"external\">Full report</a></p>",
	"notes": "drowning, unreality",
	"file": "Wintersheimer"
	},
	{
	"code": "01936",
	"heading": "<cite>SCP-1936: &ldquo;Daleport&rdquo;</cite>",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-1936\" rel=\"external\">Full report</a></p>",
	"notes": "body horror, child sacrifice, trepanning",
	"file": "Daleport"
	},
	{
	"code": "01981",
	"heading": "<cite>SCP-1981: &ldquo;Reagan Cut Up&rdquo;</cite>",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-1981\" rel=\"external\">Full report</a> (<cite><a href=\"https://scp-wiki.wikidot.com/dr-robinsons-statement\" rel=\"external\">Dr. Robinson's statement</a></cite>)</p>",
	"notes": "dementia, drug overdose, extensive descriptions of gore, genital mutilation, mentions of sexual abuse, mentions of war, non-consensual application of drugs",
	"file": "Reagan"
	},
	{
	"code": "02264",
	"heading": "<cite>SCP-2264: &ldquo;In the Court of Alagadda&rdquo;</cite>",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-2264\" rel=\"external\">Full report</a></p>",
	"notes": "torture",
	"file": "Alagadda"
	},
	{
	"code": "02317",
	"heading": "<cite>SCP-2317: &ldquo;Door to Another World&rdquo;</cite>",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-2317\" rel=\"external\">Full report</a> (<cite><a href=\"https://scp-wiki.wikidot.com/beneath-two-trees\" rel=\"external\">Beneath Two Trees</a></cite>)</p>",
	"notes": "animal sacrifice",
	"file": "Another"
	},
	{
	"code": "03000",
	"heading": "<cite>SCP-3000: &ldquo;Anantashesha&rdquo;</cite>",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-3000\" rel=\"external\">Full report</a></p>",
	"notes": "false memories, memory loss, suicide",
	"file": "Anantashesha"
	},
	{
	"code": "03034",
	"heading": "<cite>SCP-3034: &ldquo;The Counting Station&rdquo;</cite>",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-3034\" rel=\"external\">Full report</a> (<cite><a href=\"https://scp-wiki.wikidot.com/someareborntoendlessnight\" rel=\"external\">Some Are Born To Endless Night</a></cite>)</p>",
	"notes": "child abduction, child death, children in distress, darkness, memory loss",
	"file": "Counting"
	},
	{
	"code": "03241",
	"heading": "<cite>SCP-3241: &ldquo;SS Sommerfeld&rdquo;</cite>",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-3241\" rel=\"external\">Full report</a></p>",
	"notes": "body horror, descriptions of gore",
	"file": "Sommerfeld"
	},
	{
	"code": "03300",
	"heading": "<cite>SCP-3300: &ldquo;The Rain&rdquo;</cite>",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-3300\" rel=\"external\">Full report</a></p>",
	"notes": "suicide, uncanny valley, wasting disease",
	"file": "Rain"
	},
	{
	"code": "03426",
	"heading": "<cite>SCP-3426: &ldquo;Reckoner&rdquo;</cite>",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-3426\" rel=\"external\">Full report</a></p>",
	"notes": "",
	"file": "Reckoner"
	},
	{
	"code": "03673",
	"heading": "<cite>SCP-3673: &ldquo;But When The Door is Closed&rdquo;</cite>",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-3673\" rel=\"external\">Full report</a></p>",
	"notes": "body horror, child death, children in danger, claustrophobia, crushing death, extensive descriptions of gore, invisibility, suicide (by gun), unreality",
	"file": "Closed"
	},
	{
	"code": "03890",
	"heading": "<cite>SCP-3890: &ldquo;Forget Me Not&rdquo;</cite>",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-3890\" rel=\"external\">Full report</a></p>",
	"notes": "being hunted, isolation, memory loss, mention of violence to a child, uncanny valley",
	"file": "Forget"
	},
	{
	"code": "03930",
	"heading": "<cite>SCP-3930: &ldquo;The Pattern Screamer&rdquo;</cite>",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-3930\" rel=\"external\">Full report</a></p>",
	"notes": "",
	"file": "Screamer"
	},
	{
	"code": "03935",
	"heading": "<cite>SCP-3935: &ldquo;The Thing a Quiet Madness Made&rdquo;</cite>",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-3935\" rel=\"external\">Full report</a></p>",
	"notes": "claustrophobia, panic, suicide",
	"file": "Quiet"
	},
	{
	"code": "04935",
	"heading": "<cite>SCP-4935: &ldquo;Hereafter&rdquo;</cite>",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-4935\" rel=\"external\">Full report</a></p>",
	"notes": "",
	"file": "Hereafter"
	},
	{
	"code": "05005",
	"heading": "<cite>SCP-5005: &ldquo;Lamplight&rdquo;</cite>",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-5005\" rel=\"external\">Full report</a></p>",
	"notes": "body horror, darkness, depression, suicide",
	"file": "Lamplight"
	},
	{
	"code": "06439",
	"heading": "<cite>SCP-6439: &ldquo;October 32&rdquo;</cite></h4>",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-6439\" rel=\"external\">Full report</a></p>",
	"notes": "",
	"file": "32nd"
	}
]
},
{
"code": "SD",
"heading": "<cite><abbr>SCP</abbr> Database</cite>",
"blurb": "<p><q>Secure. Contain. Protect.</q> Readings of the records and tales of the <a href=\"https://scp-wiki.wikidot.com\" rel=\"external\"><abbr>SCP</abbr> Foundation</a>, produced by the <abbr>SCP</abbr> Data Podcast.</p><!--show titles are taken from the podcast, not the SCP Foundation wiki (some titles differ between the two sources)-->",
"source": "<a href=\"https://www.scpdatapodcast.com\" rel=\"external\">scpdatapodcast.com</a>, used under the <a href=\"https://creativecommons.org/licenses/by-sa/3.0\" rel=\"external license\">CC BY-SA 3.0 Unported license</a>",
"shows": [
	{
	"code": "00002-00015-00064-00084",
	"heading": "selection: Anomalous Structures",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-002\" rel=\"external\">Full report (<abbr>SCP</abbr>-002: The &ldquo;Living&rdquo; Room)</a><br><a href=\"https://scp-wiki.wikidot.com/scp-015\" rel=\"external\">Full report (<abbr>SCP</abbr>-015: Pipe Nightmare)</a><br><a href=\"https://scp-wiki.wikidot.com/scp-064\" rel=\"external\">Full report (<abbr>SCP</abbr>-064: Flawed von Neumann Structure)</a><br><a href=\"https://scp-wiki.wikidot.com/scp-084\" rel=\"external\">Full report (<abbr>SCP</abbr>-084: Static Tower)</a></p>",
	"notes": "body horror, mind control",
	"file": "Living-Pipe-Neumann-Static"
	},
	{
	"code": "00021-00099-01155",
	"heading": "selection: Anomalous Artworks",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-021\" rel=\"external\">Full report (<abbr>SCP</abbr>-021: Skin Wyrm)</a><br><a href=\"https://scp-wiki.wikidot.com/scp-099\" rel=\"external\">Full report (<abbr>SCP</abbr>-099: The Portrait)</a><br><a href=\"https://scp-wiki.wikidot.com/scp-1155\" rel=\"external\">Full report (<abbr>SCP</abbr>-1155: Predatory Street Art)</a></p>",
	"notes": "animal attack, being watched, body horror, gore, mention of suicide",
	"file": "Wyrm-Portrait-Street"
	},
	{
	"code": "00087",
	"heading": "<cite>SCP-087 - The Stairwell</cite>",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-087\" rel=\"external\">Full report</a></p>",
	"notes": "child in distress, darkness, isolation, panic",
	"file": "Stairwell"
	},
	{
	"code": "00097",
	"heading": "<cite>SCP-097 - Old Fairgrounds</cite>",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-097\" rel=\"external\">Full report</a></p>",
	"notes": "child death, gore, mind control",
	"file": "Fairgrounds"
	},
	{
	"code": "01437",
	"heading": "<cite>SCP-1437 - A Hole to Another Place</cite>",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-1437\" rel=\"external\">Full report</a></p>",
	"notes": "",
	"file": "Hole"
	},
	{
	"code": "01733",
	"heading": "<cite>SCP-1733 - Season Opener</cite>",
	"blurb": "<p><a href=\"https://scp-wiki.wikidot.com/scp-1733\" rel=\"external\">Full report</a></p>",
	"notes": "child endangerment, confinement, human sacrifice",
	"file": "Opener"
	}
]
},
{
"code": "SET",
"heading": "<cite>Seeing Ear Theater</cite>",
"blurb": "<p>A turn-of-the-millennium online sci-fi and horror radio play revival that produced both originals and adaptations.</p><!--episode numbers are taken from the internet archive (from actual file names, not the numbers in the audio player's file list), but may be incorrect-->",
"source": "<a href=\"https://archive.org/details/SETheater\" rel=\"external\">Internet Archive</a>",
"shows": [
	{
	"code": "12",
	"heading": "#12: <cite>An Elevator and a Pole</cite>, part 1",
	"blurb": "<p>One group gathers round a weird pole in the middle of nowhere; another's stuck in an elevator. They all struggle to understand and control their fates.</p>",
	"notes": "broken neck, mental breakdown, falling elevator",
	"file": "Pole-pt1"
	},
	{
	"code": "13",
	"heading": "#13: <cite>An Elevator and a Pole</cite>, part 2",
	"blurb": "<p>One group gathers round a weird pole in the middle of nowhere; another's stuck in an elevator. They all struggle to understand and control their fates.</p>",
	"notes": "descriptions of gore, fall death, vomiting, suicide",
	"file": "Pole-pt2"
	},
	{
	"code": "18",
	"heading": "#18: <cite>Titanic Dreams</cite>",
	"blurb": "<p>A survivor of the Titanic drifts into the future, where she contemplates her regrets of that disastrous night. Adapted from a story by Robert Olen Butler.</p>",
	"notes": "suicide?",
	"file": "Titanic"
	},
	{
	"code": "29",
	"heading": "#29: <cite>Facade</cite>",
	"blurb": "<p>Young hotshot advertisers snort their dead friend's ashes to receive creative inspiration, to bring her back, to help their pitches, to take over their lives.</p>",
	"notes": "ableism, addiction, car accident, human sacrifice, possession",
	"file": "Facade"
	},
	{
	"code": "33-34",
	"heading": "#33 and #34: <cite>Diary of a Mad Deity</cite>",
	"blurb": "<p>A hack horror writer, with his psychiatrist's help, tries to merge his myriad personalities into one person. Adapted from a story by James K.&thinsp;Morrow.</p>",
	"notes": "anti-Black and anti-Jewish racial slurs, child death, child torture, descriptions or gore, homophobia, lynching, mental breakdown",
	"file": "Diary"
	},
	{
	"code": "36",
	"heading": "#36: <cite>In the Shade of the Slowboat Man</cite>",
	"blurb": "<p>A vampire returns to her mortal love on his nursing-home deathbed to reminisce and say goodbye. Adapted from a story by Dean Wesley Smith.</p>",
	"notes": "abandonment, infertility",
	"file": "Shade"
	},
	{
	"code": "37",
	"heading": "#37: <cite>Greedy Choke Puppy</cite>",
	"blurb": "<p>A fiery student of Trinidadian folklore investigates the tale of the soucouyant, a skin-stealing vampire her grandma warns about. Adapted from a story by Nalo Hopkinson.</p>",
	"notes": "infanticide",
	"file": "Choke"
	},
	{
	"code": "43-44",
	"heading": "#43 and #44: <cite>Emily 501</cite>",
	"blurb": "<p>An exo-archaeologist discovers that the ancient language she's found isn't as dead as it seems.</p>",
	"notes": "body horror, language loss, mental breakdown",
	"file": "Emily"
	},
	{
	"code": "45-46",
	"heading": "#45 and #46: <cite>Marilyn or the Monster</cite>",
	"blurb": "<p>An insomniac Vietnam war veteran struggles with unseen horrors that his psychiatrist tries to unravel. Adapted from a story by Jack Dann.</p>",
	"notes": "anti-Vietnamese and anti-Black racial slurs, gore, gunshots and explosions (21:34&ndash;23:16, 38:04&ndash;38:38, 40:03, 40:33, 41:46, 42:17), sexual fantasy, survivor's guilt",
	"file": "Marilyn"
	},
	{
	"code": "53-54",
	"heading": "#53 and #54: <cite>Propagation of Light in a Vacuum</cite>",
	"blurb": "<p>A space traveller struggles to stay sane in the world beyond the speed of light&mdash;with the help of his imaginary wife.</p>",
	"notes": "drug overdose, murder-suicide, stabbing, starvation",
	"file": "Propagation"
	},
	{
	"code": "55-74",
	"heading": "#55 and #74: <cite>Meet the Neighbor</cite> and <cite>Too Late</cite>",
	"blurb": "<p>Two stories told almost entirely with sound effects and with<em>out</em> coherent speech.</p>",
	"notes": "animal attack, extended sounds of violence",
	"file": "Neighbor-Late"
	},
	{
	"code": "85",
	"heading": "#85: <cite>The Oblivion Syndrome</cite>",
	"blurb": "<p>A void-cartographer gives up on survival when his ship is damaged, leaving it to the ship's <abbr class=\"unmodified-abbr\">AI</abbr> and an interstellar freakshow to restore his will to live.</p>",
	"notes": "ableism, voice removal",
	"file": "Syndrome"
	},
	{
	"code": "88",
	"heading": "#88: <cite>Into The Sun</cite>",
	"blurb": "<p>A starship doctor finds himself alone as his vessel races into the closest star. Starring Mark Hamill.</p>",
	"notes": "helplessness, live burial",
	"file": "Sun"
	}
]
},
{
"code": "SNM",
"heading": "<cite>Sleep No More</cite>",
"blurb": "<p>A 1956&ndash;57 anthology of short horror stories read by actor Nelson Olmsted after tightening budgets started to make full radio dramas infeasible.</p>",
"source": "<a href=\"https://archive.org/details/sleep_no_more_radio\" rel=\"external\">Internet Archive</a>",
"shows": [
	{
	"code": "05",
	"heading": "#5: <cite>Over the Hill</cite> and <cite>The Man in the Black Hat</cite>",
	"blurb": "<p>A delusional man &ldquo;escapes&rdquo; his nagging wife. A gambler meets a stranger who gives him unnatural luck. Written by Michael Fessier.</p>",
	"notes": "guillotine death, implied domestic violence",
	"file": "Hill-Hat"
	},
	{
	"code": "11",
	"heading": "#11: <cite>I Am Waiting</cite> and <cite>Browdian Farm</cite>",
	"blurb": "<p>A self-named failure develops future sight. Two men uncover their holiday home's dark past. Written by Christopher Isherwood and A.&thinsp;M.&thinsp;Burrage.</p>",
	"notes": "hanging",
	"file": "Waiting-Farm"
	},
	{
	"code": "15",
	"heading": "#15: <cite>Thus I Refute Beelzy</cite><!--Note: show title in source uses the wrong name (\"Bealsley\" instead of \"Beelzy\")--> and <cite>The Bookshop</cite>",
	"blurb": "<p>A boy's imaginary friend takes offense at his cruel father. A struggling writer finds a shop of impossible books. Written by John Collier and Nelson S.&thinsp;Bond.</p>",
	"notes": "child abuse, dismemberment, traffic accident",
	"file": "Beelzy-Book"
	},
	{
	"code": "17",
	"heading": "#17: <cite>The Woman in Gray</cite> and <cite>A Suspicious Gift</cite>",
	"blurb": "<p>A man invents a spectre of hatred. A stranger gives a too-perfect gift. Written by Walker G.&thinsp;Everett and Algernon Blackwood.</p>",
	"notes": "apparent suicide, fall death, traffic accidents",
	"file": "Gray-Gift"
	}
]
},
{
"code": "Sus",
"heading": "<cite>Suspense</cite>",
"blurb": "<p>A 1940&ndash;62 anthology made by a bevy of talent. Most shows featured ordinary people thrust into suspenseful&mdash;even supernatural&mdash;situations.</p>",
"source": "<a href=\"https://archive.org/details/OTRR_Suspense_Singles\" rel=\"external\">Internet Archive</a>",
"shows": [
	{
	"code": "000",
	"heading": "#0: <cite>The Lodger</cite>",
	"blurb": "<p>A severe, religious lodging-house guest falls under suspicion of murder. Directed by Alfred Hitchcock, starring Herbert Marshall.</p>",
	"notes": "",
	"file": "Lodger"
	},
	{
	"code": "011",
	"heading": "#11: <cite>The Hitch-Hiker</cite>",
	"blurb": "<p>A man driving across the <abbr class=\"unmodified-abbr\">US</abbr> sees the same hitch-hiker calling for him again and again&hellip; and again&hellip; Written by Lucille Fletcher, introduction by and starring Orson Welles.</p>",
	"notes": "car crash, obsession, traffic death",
	"file": "Hitch-hiker"
	},
	{
	"code": "059",
	"heading": "#59: <cite>The Most Dangerous Game</cite>",
	"blurb": "<p>A big-game hunter has the tables turned when he washes up on an island owned by a man-hunter. Starring Orson Welles.</p>",
	"notes": "ableism, animal death, man devoured by dogs, stab death",
	"file": "Game"
	},
	{
	"code": "087",
	"heading": "#87: <cite>The Marvellous Barastro</cite>",
	"blurb": "<p>The world's second-greatest magician announces his plan to take revenge on the first&mdash;his mirror image. Starring Orson Welles.</p>",
	"notes": "alcohol adverts, betrayal, stalking, strangulation",
	"file": "Barastro"
	},
	{
	"code": "092",
	"heading": "#92: <cite>Donovan's Brain</cite>, part 1",
	"blurb": "<p>A scientist rescues a wealthy businessman's life by preserving his brain&mdash;only to fall under its malign sway. Starring Orson Welles.</p>",
	"notes": "alcohol adverts, animal bite, animal death, animal experimentation, betrayal, human experimentation, institutionalisation, mind control, paranoia",
	"file": "Donovan-pt1"
	},
	{
	"code": "093",
	"heading": "#93: <cite>Donovan's Brain</cite>, part 2",
	"blurb": "<p>A scientist rescues a wealthy businessman's life by preserving his brain&mdash;only to fall under its malign sway. Starring Orson Welles.</p>",
	"notes": "alcohol adverts, betrayal, human experimentation, institutionalisation, mind control, injection, non-consensual surgery, strangulation, suicide?",
	"file": "Donovan-pt2"
	},
	{
	"code": "094",
	"heading": "#94: <cite>Fugue in C Minor</cite>",
	"blurb": "<p>A music-loving widower remarries a woman who shares his passion&hellip; his children have other ideas. Written by Lucille Fletcher, starring Vincent Price and Ida Lupino.</p>",
	"notes": "alcohol adverts, asphyxiation, claustrophobia, heart attack",
	"file": "Fugue"
	},
	{
	"code": "143",
	"heading": "#143: <cite>August Heat</cite>",
	"blurb": "<p>Two strangers share premonitions of their fates on a brutally hot August day. Starring Ronald Colman.</p>",
	"notes": "alcohol adverts",
	"file": "August"
	},
	{
	"code": "158",
	"heading": "#158: <cite>The Furnished Floor</cite>",
	"blurb": "<p>A happy widower returns to the house he shared with his wife and buys back all their possessions, to his landlady's concern. Written by Lucille Fletcher.</p>",
	"notes": "alcohol adverts, animal death, breach of privacy, darkness, eviction",
	"file": "Furnished"
	}, 
	{
	"code": "165",
	"heading": "#165: <cite>The Dunwich Horror</cite>",
	"blurb": "<p>A union of human woman and alien god produces terrible, eldritch offspring. Adapted from a story by H.&thinsp;P.&thinsp;Lovecraft, starring Robert Colman.</p>",
	"notes": "ableism, alcohol adverts, animal attack, body horror, cattle mutilation",
	"file": "Dunwich"
	},
	{
	"code": "222",
	"heading": "#222: <cite>The House in Cypress Canyon</cite>",
	"blurb": "<p>A couple takes possession of a renovated house, only to find a dark presence taking possession of <em>them</em>.</p>",
	"notes": "alcohol adverts, bite injury, blood, murder-suicide",
	"file": "Cypress"
	},
	{
	"code": "224",
	"heading": "#224: <cite>The Thing in the Window</cite>",
	"blurb": "<p>An actor claims he sees a dead body in the flat across the road; everyone else thinks he's mad. Written by Lucille Fletcher, starring Joseph Cotton.</p>",
	"notes": "alcohol adverts, betrayal, gaslighting, harassment, suicide",
	"file": "Window"
	},
	{
	"code": "259",
	"heading": "#259: <cite>Murder Aboard the Alphabet</cite>",
	"blurb": "<p>A ship's crew find their captain's obsession with orderliness harmless until they start disappearing&mdash;in alphabetical order.</p>",
	"notes": "alcohol adverts, disappearance at sea, implied execution, imprisonment, sanism",
	"file": "Alphabet"
	},
	{
	"code": "300",
	"heading": "#300: <cite>The Yellow Wallpaper</cite>",
	"blurb": "<p>A woman confined by her husband after a nervous breakdown starts to see things behind the wallpaper. Adapted from a story by Charlotte Perkins Gilman.</p>",
	"notes": "domestic abuse, mental breakdown",
	"file": "Wallpaper"
	},
	{
	"code": "346",
	"heading": "#346: <cite>Ghost Hunt</cite>",
	"blurb": "<p>A skeptical radio <abbr class=\"unmodified-abbr\">DJ</abbr> and a psychic investigator spend a night in a house nicknamed the &ldquo;Death Trap&rdquo;.</p>",
	"notes": "blood, hallucination?, suicide?",
	"file": "Ghost"
	},
	{
	"code": "379",
	"heading": "#379: <cite>Salvage</cite>",
	"blurb": "<p>A pilot, his ex, and her husband get tangled in a fraught expedition to recover sunken gold from the Caribbean. Starring Van Johnson.</p>",
	"notes": "betrayal, gunshots (15:23, 20:05, 20:34&ndash;20:40), plane crash",
	"file": "Salvage"
	},
	{
	"code": "550",
	"heading": "#550: <cite>The Giant of Thermopylae</cite>",
	"blurb": "<p>A brawler gets trapped in a funfair until he can beat the menacing mechanical colossus: the Giant of Thermopylae. Starring Frank Lovejoy.</p>",
	"notes": "",
	"file": "Thermopylae"
	},
	{
	"code": "625",
	"heading": "#625: <cite>Classified Secret</cite>",
	"blurb": "<p>Spies match money, wits, and steel on an interstate bus, with deadly military secrets in the balance.</p>",
	"notes": "gunshots (21:36&ndash;37, 21:48)",
	"file": "Classified"
	},
	{
	"code": "648",
	"heading": "#648: <cite>The Waxwork</cite>",
	"blurb": "<p>A nervy, desperate freelance journalist resolves to write about spending the night in a museum of wax serial killers. Starring William Conrad.</p>",
	"notes": "anxiety, claustrophobia, hallucinations?, hypnosis, panic attack, paranoia",
	"file": "Waxwork"
	},
	{
	"code": "672",
	"heading": "#672: <cite>The Signalman</cite>",
	"blurb": "<p>Spectres cryptically warn a railway signalman of disaster&mdash;or maybe he's just going mad. Adapted from a story by Charles Dickens.</p>",
	"notes": "train death",
	"file": "Signalman"
	},
	{
	"code": "673",
	"heading": "#673: <cite>Three Skeleton Key</cite>",
	"blurb": "<p>Three lighthouse keepers fall under siege from a terrifying swarm of rats. Starring Vincent Price.</p>",
	"notes": "ableism, animal deaths, bite injury, claustrophobia, isolation, mental breakdown",
	"file": "Key"
	},
	{
	"code": "674",
	"heading": "#674: <cite>The Long Night</cite>",
	"blurb": "<p>A night shift air traffic controller must help a lost, scared, untested pilot who's running out of fuel and trapped above the clouds. Starring Frank Lovejoy.</p>",
	"notes": "implied child death, parental recklessness, smoking",
	"file": "Night"
	},
	{
	"code": "688",
	"heading": "#688: <cite>Present Tense</cite>",
	"blurb": "<p>A death row inmate escapes execution via clock-stopping delusion. Starring Vincent Price.</p>",
	"notes": "axe murder, gas chamber, hijacking, home invasion, train derailment",
	"file": "Present"
	},
	{
	"code": "689",
	"heading": "#689: <cite>The Peralta Map</cite>",
	"blurb": "<p>Treasure hunters contract a local guide to help them find an ancient, legendary, <em>haunted</em> gold mine.</p>",
	"notes": "abandonment, betrayal, fall injury, gunshots (25:25&ndash;25:27, 27:29&ndash;27:32)",
	"file": "Peralta"
	},
	{
	"code": "799",
	"heading": "#799: <cite>Deep, Deep is My Love</cite>",
	"blurb": "<p>A diver seeks solitude from his wife in the embrace of a golden lady under the sea.</p>",
	"notes": "animal death, asphyxiation, hallucination?, narcosis",
	"file": "Deep"
	},
	{
	"code": "805",
	"heading": "#805: <cite>The Pit and the Pendulum</cite>",
	"blurb": "<p>A man falls into the hands of the Spanish Inquisition and their intricate psychological torture devices.</p>",
	"notes": "",
	"file": "Pendulum"
	},
	{
	"code": "878",
	"heading": "#878: <cite>The Green Lorelei</cite>",
	"blurb": "<p>A writer becomes obsessed with a woman singing in the apartment upstairs, though the old man living there swears his wife is dead.</p>",
	"notes": "deceptively gaining entry to another person's home, eviction, institutionalisation, ransacking",
	"file": "Lorelei"
	},
	{
	"code": "901",
	"heading": "#901: <cite>The Black Door</cite>",
	"blurb": "<p>An archaeologist and his local guide seek an ancient Central American city and carelessly disturb what lies beyond the black door at its heart.</p>",
	"notes": "gunshots (16:41, 16:59, 17:43), racism",
	"file": "Door"
	},
	{
	"code": "916",
	"heading": "#916: <cite>Heads You Lose</cite>",
	"blurb": "<p>Detectives take a lucrative case to find a terminally-ill embezzler years after he should've died, and find themselves in over their heads.</p>",
	"notes": "cigarette ads, gunshots (19:08&ndash;19:29), entombment, suicide",
	"file": "Heads"
	},
	{
	"code": "927",
	"heading": "#927: <cite>That Real Crazy Infinity</cite>",
	"blurb": "<p>Two beatniks out of money take on a simple delivery job that ends up involving esoteric electronics and the voices of the past.</p>",
	"notes": "explosion (20:02&ndash;20:09)",
	"file": "Infinity"
	}
]
},
{
"code": "TF",
"heading": "<cite>Theater 5</cite>",
"blurb": "<p>A 1964&ndash;65 anthology of radio dramas broadcast by the <abbr title=\"American Broadcasting Company\">ABC</abbr> in an attempted revival of the radio play tradition after the rise of television.</p>",
"source": "<a href=\"https://archive.org/details/OTRR_Theater_Five_Singles\" rel=\"external\">Internet Archive</a>",
"shows": [
	{
	"code": "021",
	"heading": "#21: <cite>The Scream</cite>",
	"blurb": "<p>A demolition crew has a deadly encounter while tearing down a venerable old house.</p>",
	"notes": "alcohol, workplace intimidation",
	"file": "Scream"
	},
	{
	"code": "106",
	"heading": "#106: <cite>Five Strangers</cite>",
	"blurb": "<p>Five stranded strangers use a cargo plane to beat flight delays when dense fog grounds all other craft, unaware of the fate awaiting them.</p>",
	"notes": "plane crash",
	"file": "Strangers"
	},
	{
	"code": "154",
	"heading": "#154: <cite>The Land of Milk and Honey</cite>",
	"blurb": "<p>A sinner ends up in heaven, where he gets all he ever wanted and never has to lift a finger&mdash;an exasperating paradise.</p>",
	"notes": "alcohol, betrayal, gunshots (5:32, 17:08)",
	"file": "Honey"
	}
]
},
{
"code": "WC",
"heading": "<cite>The Weird Circle</cite>",
"blurb": "<p>A 1943&ndash;45 anthology that adapted classic horror and supernatural tales to the airwaves, with low budgets limiting the use of music and sound effects.</p>",
"source": "<a href=\"https://archive.org/details/OTRR_Weird_Circle_Singles\" rel=\"external\">Internet Archive</a>",
"shows": [
	{
	"code": "43",
	"heading": "#43: <cite>The Bell Tower</cite>",
	"blurb": "<p>An arrogant architect brings doom upon himself in the creation of an &ldquo;impossible&rdquo; bell tower. Adapted from a story by Herman Melville.</p>",
	"notes": "bludgeon death, crush death",
	"file": "Bell"
	},
	{
	"code": "63",
	"heading": "#63: <cite>The Ancient Mariner</cite>",
	"blurb": "<p>An old mariner recounts how slaying an albatross brought a fatal curse upon his crew. Adapted from a poem by Samuel Taylor Coleridge.</p>",
	"notes": "animal death, starvation",
	"file": "Mariner"
	}
]
},
{
"code": "WP",
"heading": "<cite>The White People</cite>",
"blurb": "<p>A girl's diary records how she stepped into a deadly world of black magic in the countryside. Written by Arthur Machen, first published in 1904, and read by Ian Virly for LibriVox.</p>",
"source": "<a href=\"https://librivox.org/lovecrafts-influences-and-favorites-by-various\" rel=\"external\">LibriVox</a>",
"shows": [
	{
	"code": "1",
	"heading": "part 1",
	"blurb": "<p>The reclusive Ambrose and his friend, Cotgrave, debate good and evil, and Ambrose lends Cotgrave a curious green diary that he begins to read.</p>",
	"notes": "Christian supremacism, finger injury, injured child",
	"file": "Prologue"
	},
	{
	"code": "2",
	"heading": "part 2",
	"blurb": "<p>Cotgrave continues to read the young girl's diary&mdash;her account of her growing exploration of fey and magical things.</p>",
	"notes": "Christian supremacism",
	"file": "Green"
	},
	{
	"code": "3",
	"heading": "part 3",
	"blurb": "<p>Cotgrave finishes the incomplete diary and returns it to Ambrose, and they discuss the girl's fate.</p>",
	"notes": "child death, Christian supremacism, finger injury, injured child",
	"file": "Epilogue"
	}
]
},
{
"code": "Wil",
"heading": "<cite>The Willows</cite>",
"blurb": "<p>Two travellers become trapped on a river island in an eerie sea of willows where the walls of reality are fragile and vast things peer through. Written by Algernon Blackwood, first published in 1907, and read by Phil Chenevart for LibriVox.</p>",
"source": "<a href=\"https://librivox.org/the-willows-by-algernon-blackwood-2\" rel=\"external\">LibriVox</a>",
"shows": [
	{
	"code": "1",
	"heading": "part 1",
	"blurb": "<p>The narrator and his companion arrive at the island and shrug off warnings to leave.</p>",
	"notes": "",
	"file": "pt1"
	},
	{
	"code": "2",
	"heading": "part 2",
	"blurb": "<p>Night falls and premonitions of eerie doom begin.</p>",
	"notes": "",
	"file": "pt2"
	},
	{
	"code": "3",
	"heading": "part 3",
	"blurb": "<p>Disaster as the travellers' supplies go missing and their boat is damaged by unknown forces.</p>",
	"notes": "",
	"file": "pt3"
	},
	{
	"code": "4",
	"heading": "part 4",
	"blurb": "<p>The horror strikes directly and the travellers confront a world beyond their own.</p>",
	"notes": "",
	"file": "pt4"
	}
]
},
{
"code": "WT",
"heading": "<cite>The Witch's Tale</cite>",
"blurb": "<p>The first broadcast horror anthology (from 1931&ndash;38), written by Alonzo Deen Cole and hosted by the witch &ldquo;Old Nancy&rdquo; and her black cat, Satan.</p>",
"source": "<a href=\"https://radioechoes.com/?page=series&genre=OTR-Thriller&series=The%20Witchs%20Tale\" rel=\"external\">Radio Echoes</a>",
"shows": [
	{
	"code": "041",
	"heading": "#41: <cite>The Wonderful Bottle</cite>",
	"blurb": "<p>A tramp buys a bottle that grants any wish&mdash;but his soul's forfeit if he can't sell it for <em>less</em> than he paid for it. Adapted from a story by Robert Louis Stevenson.</p>",
	"notes": "leprosy, parental death",
	"file": "Bottle"
	},
	{
	"code": "116",
	"heading": "#116: <cite>Le Mannequinne</cite>",
	"blurb": "<p>A woman becomes paranoid that her husband's new artistic mannequin wants to replace her.</p>",
	"notes": "head wound, obsession, stab death",
	"file": "Mannequinne"
	}
]
},
{
"code": "WBP",
"heading": "<cite>With Book and Pipe</cite>",
"blurb": "<p>A mostly-lost anthology of radio stories probably broadcast in the 1940s, narrated by &ldquo;the Man with Book and Pipe&rdquo;; only one episode survives.</p>",
"source": "<a href=\"https://archive.org/details/UniqueOldTimeRadioEpisodes/With+Book+and+Pipe+1943.mp3\" rel=\"external\">Internet Archive</a>",
"shows": [
	{
	"code": "X",
	"heading": "#X: <cite>The Graveyard Rats</cite>",
	"blurb": "<p>A grave robber's work takes him into the burrows of a swarm of unnaturally large and intelligent rats. Adapted from a story by Henry Kuttner.</p>",
	"notes": "animal bite, asphyxiation, claustrophobia, entombment",
	"file": "Rats"
	}
]
},
{
"code": "XMO",
"heading": "<cite>X Minus One</cite>",
"blurb": "<p>5, 4, 3, 2&hellip; X minus 1. A 1955&ndash;58 anthology of original and adapted sci-fi stories, mostly scripted by Ernest Kinoy and George Lefferts; the successor to their earlier series <cite>Dimension X</cite>.</p>",
"source": "<a href=\"https://archive.org/details/OTRR_X_Minus_One_Singles\" rel=\"external\">Internet Archive</a>",
"shows": [
	{
	"code": "037",
	"heading": "#37: <cite>The Cave of Night</cite>",
	"blurb": "<p>The world unites to rescue an astronaut adrift in <q>the cave of night</q>&mdash;but he may not want to return. Adapted from a story by James E.&thinsp;Gunn.</p>",
	"notes": "suicide",
	"file": "Cave"
	},
	{
	"code": "039",
	"heading": "#39: <cite>Skulking Permit</cite>",
	"blurb": "<p>A utopian space colony cut off from Earth proves they're a model of Earth culture, including an official criminal. Adapted from a story by Robert Sheckley.</p>",
	"notes": "gunshots (12:31, 22:07)",
	"file": "Skulking"
	},
	{
	"code": "042",
	"heading": "#42: <cite>A Gun for Dinosaur</cite>",
	"blurb": "<p>A time-travelling hunting guide explains why he's so selective about who he helps hunt dinosaur. Adapted from a story by L.&thinsp;Sprague de Camp.</p>",
	"notes": "adultery, brawl, gunshots (7:17, 11:02, 13:18&ndash;13:20, 15:29, 18:17, 19:36&ndash;38, 20:05)",
	"file": "Dinosaur"
	},
	{
	"code": "043",
	"heading": "#43: <cite>Tunnel Under the World</cite>",
	"blurb": "<p>A man wakes up on the 15th of June over and over&mdash;in a silent town utterly dominated by advertising. Adapted from a story by Frederik Pohl.</p>",
	"notes": "bludgeoning death, paranoia, surveillance",
	"file": "Tunnel"
	},
	{
	"code": "063",
	"heading": "#63: <cite>Student Body</cite>",
	"blurb": "<p>An all-consuming, rapidly-evolving species of alien mice poses an existential challenge to humanity. Adapted from a story by F.&thinsp;L.&thinsp;Wallace.</p>",
	"notes": "gunshots (21:55&ndash;57)",
	"file": "Student"
	},
	{
	"code": "065",
	"heading": "#65: <cite>Surface Tension</cite>",
	"blurb": "<p>Scientists invent a microscopic solution so humanity can live out its days before the apocalypse. Adapted from a story by James Blish.</p>",
	"notes": "",
	"file": "Surface"
	},
	{
	"code": "068",
	"heading": "#68: <cite>The Lifeboat Mutiny</cite>",
	"blurb": "<p>Two planetary surveyors buy an <abbr class=\"unmodified-abbr\" class=\"unmodified-abbr\">AI</abbr> lifeboat that once belonged to an alien navy&mdash;and doesn't know the war's over. Adapted from a story by Robert Sheckley.</p>",
	"notes": "confinement",
	"file": "Lifeboat"
	},
	{
	"code": "071",
	"heading": "#71: <cite>Colony</cite>",
	"blurb": "<p>Everyday objects turn deadly against colonists on an alien planet. Adapted from a story by Philip K.&thinsp;Dick.</p>",
	"notes": "being digested alive, gunshots (10:28, 15:24&ndash;34, 19:16), strangulation",
	"file": "Colony"
	},
	{
	"code": "092",
	"heading": "#92: <cite>The Seventh Victim</cite>",
	"blurb": "<p>After the last World War, peace is kept via man-hunt bloodsport, but not every contender is up to the task. Adapted from a story by Robert Sheckley.</p>",
	"notes": "betrayal, gunshots (2:32&ndash;52, 04:25&ndash;26, 11:46&ndash;48, 20:56)",
	"file": "Victim"
	},
	{
	"code": "101",
	"heading": "#101: <cite>The Category Inventor</cite>",
	"blurb": "<p>A musician replaced by a robot scrambles to invent a unique new job&mdash;just like everyone else. Adapted from a story by Arthur Sellings.</p>",
	"notes": "",
	"file": "Category"
	},
	{
	"code": "118",
	"heading": "#118: <cite>The Light</cite>",
	"blurb": "<p>The first astronauts on the moon find old footprints in the dust. Adapted from a story by Poul Anderson.</p>",
	"notes": "",
	"file": "Light"
	}
]
}
];