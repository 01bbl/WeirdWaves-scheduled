const oldSchedules = [
"2022-09-05-Apocalyptic Awe",
"2022-09-12-Subterranean Strangeness",
"2022-09-19-Crime and Punishment",
"2022-09-26-Creature Features",
"2022-10-03-Beta Bangers",
"2022-10-10-Horror on Wheels",
"2022-10-17-Beta Bangers 2",
"2022-10-24-Musical Malevolence",
"2022-10-31-Halloween Horror",
"2022-11-07-Weirder Waves",
"2022-11-14-Afraid Alone",
"2022-11-21-Science Has Gone Too Far!",
"2022-11-28-Living Warmth",
"2022-12-05-Cold Creeps In&hellip;",
"2022-12-12-Murderous Heat",
"2022-12-19-Random Week",
"2022-12-26-Dark Faith",
"2023-01-02-Creatures of the Night",
"2023-01-09-Strange Mechanisms",
"2023-01-16-Random Week",
"2023-01-23-Serial Week",
"2023-01-30-Roadkill",
"2023-02-06-Extra-Terror-Estrial",
"2023-02-13-Romance With a Side of Terror",
"2023-02-20-Random Week",
"2023-02-27-Long and Short",
"2023-03-06-The Ancient",
"2023-03-13-Business & Pleasure",
"2023-03-20-This Place Ain't Right",
"2023-03-27-Attack of the Fifty-Foot&hellip;",
"2023-04-03-Random Week",
"2023-04-10-Radio-Meta-Horror",
"2023-04-17-Mechanical Monstrosities",
"2023-04-24-Time's A-Wastin'",
"2023-05-01-The Great Wide Open",
"2023-05-08-The Pegāna Cycle",
"2023-05-15-World Wars",
"2023-05-22-Hope, Of All Things",
"2023-05-29-Weird Words",
"2023-06-05-Wishes and Fantasies",
"2023-06-12-The Art of Performance",
"2023-06-19-Weird Waves Anniversary",
];

function writeOldSchedulesTable() {
	let rows = "";

	for (const oldSchedule of oldSchedules) {
		const date = oldSchedule.slice(0,10),
		title = oldSchedule.slice(11);

		rows += '<tr><th scope="row"><a href="./schedules/schedule-' + date + '.json"><time>' + date + "</time></a></th>" +
		"<td>" + title + "</td></tr>";
	}

	document.getElementById("loading-spinner-old-schedules")?.remove();
	document.getElementById("old-schedules").innerHTML = rows;
}

document.addEventListener("DOMContentLoaded", () => writeOldSchedulesTable());